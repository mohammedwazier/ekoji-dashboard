<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/icon', function(){
    return view('pages.icons');
})->name('icon');

Route::get('/map', function(){
    return view('pages.map');
})->name('map');

Route::get('/notification', function(){
    return view('pages.notifications');
})->name('notification');

Route::get('/tables', function(){
    return view('pages.tables');
})->name('table');

Route::get('/typography', function(){
    return view('pages.typography');
})->name('typography');


Route::get('/', 'MainController@Verify');

Route::get('/login', 'LoginController@Index')->name('Login');
Route::post('/login', 'LoginController@Login')->name('LoginProcess');

Route::get('/phpinfo', function(){
    return phpinfo();
})->name('phpinfo');

Route::group(['middleware' => 'checklogin'], function(){
    Route::prefix('dashboard')->group(function(){
        Route::get('/', 'HomepageController@Index')->name('Dashboard');
        Route::get('/logout', 'LoginController@Logout')->name('LogoutProcess');

        Route::prefix('produk')->group(function(){
            Route::get('/', 'ProdukController@Index')->name('Produk');
            Route::post('/CreateKategori', 'ProdukController@CreateKategori')->name('CreateKategori');
            Route::get('/kategori/{id}', 'ProdukController@GetSingleKategori')->name('DetailKategori');
            Route::post('/kategori/update', 'ProdukController@UpdateKategori')->name('UpdateKategori');
            Route::get('/kategori/delete/{id}', 'ProdukController@DeleteKategori')->name('DeleteKategori');
            Route::post('/produk/CreateProduk', 'ProdukController@CreateProduk')->name('CreateProduk');
            Route::post('/detail', 'ProdukController@GetSingleProduk')->name('DetailProduk');
            Route::get('/{id}', 'ProdukController@PopupProduk')->name('ProdukPopup');
            Route::post('/produk/update', 'ProdukController@UpdateProduk')->name('UpdateProduk');
            Route::get('/produk/delete/{id}', 'ProdukController@DeleteProduk')->name('DeleteProduk');

            Route::post('/produk', 'ProdukController@Search')->name('SearchProductPage');
            Route::post('/produk/placement', 'ProdukController@EditPlacementData')->name('DetailPlacement');
            Route::post('/produk/placement/save', 'ProdukController@savePlacement')->name('SavePlacement');
        });




        Route::get('/deposit', 'DepositController@Index')->name('Deposit');
        Route::post('/deposit', 'DepositController@Search')->name('SearchDeposit'); /*Belum*/
        Route::get('/deposit/detail/{id}', 'DepositController@DetailDeposit')->name('DetailDeposit');
        Route::post('/deposit/update', 'DepositController@UpdateDeposit')->name('UpdateDeposit');
        Route::post('/deposit/detail-mutasi', 'DepositController@DetailMutasiExternal')->name('popupDetailMutasi');
        Route::post('/deposit/update-deposit-transaksi', 'DepositController@UpdateDepositTransaksi')->name('UpdateDepositDetail');
        Route::post('/deposit/update-deposit-trx-wrong', 'DepositController@wrongTrfTrxDeposit')->name('WrongTrfDeposit');

        Route::get('/user', 'AccountController@Index')->name('User');
        Route::get('/user/{id}', 'AccountController@DetailUser')->name('DetailUser');
        Route::post('/user/create', 'AccountController@CreateUser')->name('CreateProfile');
        Route::post('/user', 'AccountController@Search')->name('SearchProfile');
        Route::post('/user/getDetail', 'AccountController@PopupDetail')->name('getDetailUser');
        Route::get('/user/suspend/{id}', 'AccountController@Suspend')->name('SuspendUser');
        Route::get('/user/unsuspend/{id}', 'AccountController@Unsuspend')->name('UnsuspendUser');
        Route::get('/user/delete/{id}', 'AccountController@Delete')->name('DeleteUser');

        Route::get('/otp', 'OTPController@Index')->name('Otp');

        Route::get('/cashflow', 'CashflowController@Index')->name('Cashflow');
        Route::post('/cashflow', 'CashflowController@Detail')->name('DetailCashflow');
        Route::get('/cashflow/{id}', 'CashflowController@DetailPopup')->name('PopupCashflow');

        Route::get('/transaksi', 'TransaksiController@Index')->name('Transaksi');
        Route::post('/transaksi', 'TransaksiController@Search')->name('SearchTransaction');
        Route::get('/transaksi/{id}', 'TransaksiController@DetailTransaksi')->name('TransaksiDetail'); /*Belum, Harusnya nampilkan Popup*/

        Route::get('/event', 'EventController@Index')->name('Event');
        Route::post('/event', 'EventController@Search')->name('SearchEvent');
        Route::post('/event/create', 'EventController@Create')->name('CreateEvent');
        // Route::get('/event/{id}', 'EventController@DetailEvent')->name('DetailEvent');
        Route::post('/event/getDetail', 'EventController@PopupEvent')->name('PopupEvent');
        Route::get('/event/delete/{id}', 'EventController@Delete')->name('DeleteEvent');
        Route::post('/event/update', 'EventController@Update')->name('UpdateEvent');

        Route::get('/setting', 'SettingController@Index')->name('Setting');
        Route::post('/setting/create', 'SettingController@CreateSetting')->name('CreateSetting');
        Route::post('/setting/getDetail', 'SettingController@DetailPopup')->name('PopupSetting');
        Route::post('/setting/update', 'SettingController@Update')->name('UpdateSetting');
        Route::get('/setting/delete/{kode}', 'SettingController@Delete')->name('DeleteSetting');

        Route::get('/profisiensi', 'ProfisiensiController@Index')->name('Profisiensi');
        Route::post('/profisiensi', 'ProfisiensiController@Search')->name('SearchPRofisiensi');
        Route::post('/profisiensi/create', 'ProfisiensiController@Create')->name('CreateUserProfisiensi');

        Route::get('/internal-account', 'InternalController@Index')->name('InternalAccount');

        Route::get('/userguide', 'UserGuideController@Index')->name('Userguide');
        Route::post('/userguide/create', 'UserGuideController@Create')->name('UserGuideCreate');
        Route::get('/userguide/delete/{guideid}', 'UserGuideController@Delete')->name('UserGuideDelete');
    });
});
