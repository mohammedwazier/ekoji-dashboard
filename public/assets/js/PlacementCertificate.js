/*Create Simple Library*/
var ClickID;
var yRange;
var centerX, centerY;
var image;
var heightContent, widthContent;

var data_image, data_name, data_nomor;

var fontSize;
var __globalData = {};
var __boxData = [];
var __boxSize = [];
var __centerText = [];
var __dataImage = [];
var __yRange = [];
var __centerX = [];
var __centerY = [];
var __defaultWarna = {
    nama: 'b',
    nomor: 'b'
}

var __Canvas;

$(document).click(function(e){
    if(!$(e.target).is('.clickedContent, .center-x, .center-y')){
        $(`#${ClickID}`).css('border', 'solid 1px rgba(0,0,0,0)');
        ClickID = null;
    }
})

$(document).keydown(function(e){
    if(ClickID){
        e.preventDefault();
        switch(e.which){
            case 38: {
                // UP
                moveWithArror('top', -1, 0, image.clientHeight)
            } break;
            case 39: {
                // RIGHT
                moveWithArror('left', 1, 0, image.clientWidth)
            } break;
            case 40: {
                // DOWN
                moveWithArror('top', 1, 0, image.clientHeight)
            } break;
            case 37 : {
                // LEFT
                moveWithArror('left', -1, 0, image.clientWidth)
            } break;
        }

        let id = ClickID;
        let dataType = $('#'+ClickID).attr('data-type')
        let left, top, fontSize, width, height;

        left = Number($('#'+id).css('left').substr(0, $('#'+id).css('left').length - 2));
        top = Number($('#'+id).css('top').substr(0, $('#'+id).css('top').length - 2));
        fontSize = Number($('#'+id).css('font-size').substr(0, $('#'+id).css('font-size').length - 2));
        width = $('#'+id).width() + 2;
        height = $('#'+id).height() + 2;

        let data = {
            type: dataType,
            width,
            height,
            fontSize,
            left,
            top,
        }
        __globalData[dataType] = data;

        $('#data-'+dataType).val(JSON.stringify(data));

        // console.log(dataType)

        // console.log('Global Data', __globalData)
        // console.log('Data Image', __dataImage)

        // console.log(__globalData)
    }
})

$('.colorChange').on('change', function(e){
    let value = e.target.value;
    let id = $(e.target).attr('form-target');
    id = '#'+id;

    let idDataType = $(id).attr('data-type')
    let dataValue = JSON.parse($('#data-'+idDataType).val())
    switch(value){
        case 'b': {
            $(id).css('color', '#000')
            dataValue.color = '#000';
            $('#data-'+idDataType).val(JSON.stringify(dataValue))
        } break;
        case 'w': {
            $(id).css('color', '#FFF')
            dataValue.color = '#fff';
            $('#data-'+idDataType).val(JSON.stringify(dataValue))
        } break;
    }
})

$('.fontSize').on('change', function(e){
    let value = e.target.value;
    let id = $(e.target).attr('form-target');

    // console.log('fontSize')
    id = '#'+id;
    // console.log(value, id)
    $(id).css('font-size', `${value}px`)
    updateData($(id).width(), $(id).height());

    let dataType = $(id).attr('data-type')
    let left, top, fontSize, width, height;

    left = Number($(id).css('left').substr(0, $(id).css('left').length - 2));
    top = Number($(id).css('top').substr(0, $(id).css('top').length - 2));
    fontSize = Number(value);
    width = $(id).width() + 2;
    height = $(id).height() + 2;

    let data = {
        type: dataType,
        width,
        height,
        fontSize,
        left,
        top,
    }

    __globalData[dataType] = data;

    // console.log('Global Data', __globalData)
    // console.log('Data Image', __dataImage)
    // console.log(dataType)
    // console.log(data)
    $('#data-'+dataType).val(JSON.stringify(data));

})
$('.clickedContent').on('click', function(e){
    let id = $(this).attr('id');
    ClickID = id;
    // $(this).removeClass('blink_border');
    // $('#'+id).removeClass('blink_border')
    console.log(id, this);
    $('.clickedContent').css('border', 'solid 1px rgba(0,0,0,0)');
    $(this).css('border', 'solid 1px #000')
})
$('.center-x').on('click', function(){
    let target = $(this).attr('form-target');
    let targ = $('#'+target);
    let id = target;
    let wContent = targ.width();
    $('#'+target).css({left: centerX - (wContent / 2)})

    let dataType = $('#'+target).attr('data-type')
    let left, top, fontSize, width, height;

    left = Number($('#'+id).css('left').substr(0, $('#'+id).css('left').length - 2));
    top = Number($('#'+id).css('top').substr(0, $('#'+id).css('top').length - 2));
    fontSize = Number($('#'+id).css('font-size').substr(0, $('#'+id).css('font-size').length - 2));
    width = $('#'+id).width() + 2;
    height = $('#'+id).height() + 2;

    let data = {
        type: dataType,
        width,
        height,
        fontSize,
        left,
        top,
    }
    __globalData[dataType] = data;

    // console.log('Global Data', __globalData)
    // console.log('Data Image', __dataImage)
    // console.log(dataType)
    // console.log(data)
    $('#data-'+dataType).val(JSON.stringify(data));
})
$('.center-y').on('click', function(){
    let target = $(this).attr('form-target')
    let targ = $('#'+target);
    let id = target;
    let hContent = targ.height();
    targ.css({top: centerY - (hContent / 2)})

    let dataType = $('#'+target).attr('data-type')
    let left, top, fontSize, width, height;

    left = Number($('#'+id).css('left').substr(0, $('#'+id).css('left').length - 2));
    top = Number($('#'+id).css('top').substr(0, $('#'+id).css('top').length - 2));
    fontSize = Number($('#'+id).css('font-size').substr(0, $('#'+id).css('font-size').length - 2));
    width = $('#'+id).width() + 2;
    height = $('#'+id).height() + 2;

    let data = {
        type: dataType,
        width,
        height,
        fontSize,
        left,
        top,
    }
    __globalData[dataType] = data;
    // console.log('Global Data', __globalData)
    // console.log('Data Image', __dataImage)
    // console.log(dataType)
    // console.log(data)
    $('#data-'+dataType).val(JSON.stringify(data));
})

$('.dragContent').draggable({
    start: function() {
        drawLineAllowed = true;
    },
    helper: "original",
    containment: "parent",
    stop: function(event,ui) {
        // console.log('Drag')
        let dataType = $(this).attr('data-type')
        let left, top, fontSize, width, height;
        let { offset, position } = ui;
        left = Number($(this).css('left').substr(0, $(this).css('left').length - 2));
        top = Number($(this).css('top').substr(0, $(this).css('top').length - 2));
        fontSize = Number($(this).css('font-size').substr(0, $(this).css('font-size').length - 2));
        width = $(this).width() + 2;
        height = $(this).height() + 2;

        let data = {
            type: dataType,
            width,
            height,
            fontSize,
            left,
            top,
        }

        if(position.left > __yRange[dataType].min && position.left < __yRange[dataType].max){
            let rangedVer = __centerX[dataType] - ($(this).width() / 2)
            $(this).css({left: rangedVer})
            data.left = rangedVer;
        }
        __globalData[dataType] = data;
        // console.log(data)
        $('#data-'+dataType).val(JSON.stringify(data));
        // console.log('Global Data', __globalData)
        // console.log('Data Image', __dataImage)
        // console.log(dataType)
    }
})

function updateLoad(imageData){
	$('.dragContent').css('color', '#000');
    image = document.getElementById(imageData.target);
    // console.log('image',image.clientWidth)
    // let parentCanvas = $(`#${imageData.target}`).parent()
    // let boxText = parentCanvas.find('.clickedContent');

    let parentData = $('#'+imageData.target).parents('#parentCanvas')
    parentData = parentData[0];
    let boxText = $(parentData).find('.clickedContent');

    for(let idx = 0; idx < boxText.length; idx++){
    	var elData = $(boxText[idx])
    	let dataType = elData.attr('data-type')
    	__boxData[dataType] = boxText[idx];
    	widthContent = $(boxText[idx]).width();
    	heightContent = $(boxText[idx]).height();

    	centerX = imageData.width / 2;
    	centerY = imageData.height / 2;

    	__centerX[dataType] = centerX;
        __centerY[dataType] = centerY;

    	var xLineCenter = centerX - (widthContent / 2);
    	var yLineCenter = centerY - (heightContent / 2);

    	 let dataImage = {
	        original: {
	            width: imageData.width,
	            height: imageData.height
	        },
	        originalRatio: {
	            w: imageData.ratioWidth,
	            h: imageData.ratioHeight
	        },
	        resizeTransform: {
	            width: 0,
	            height: 0
	        },
	        resize: false,
	        resizeScale: {
	            width: 0,
	            height: 0
	        }
	    }
	    __dataImage[dataType] = dataImage

        var marginRange = imageData.width * (2 / 100);
        yRange = {
            min: centerY - marginRange,
            max: centerY + marginRange
        }

        __yRange[dataType] = yRange

	    let dataNomor = ['PREI', 'SEM', 'ERP', 'VI', 2020, '0001'];
	    if(imageData.width > 700){
	        let bufferImageWidth = imageData.width;
	        let bufferImageHeight = imageData.height;

	        image = document.getElementById(imageData.target);
            if(parseInt(imageData.height) === parseInt(image.height)){
                image.height = image.height / (imageData.width / image.width)
            }
            // console.log('Pengaturan harusnyaa',image, gcd(image.width, image.height), gcd(imageData.width, imageData.height))

	        r = gcd(imageData.width, imageData.height)

	        dataImage = {
	            ...dataImage,
	            resize: true,
	            original: {
	                width: bufferImageWidth,
	                height: bufferImageHeight
	            },
	            resizeTransform: {
	                width: image.width,
	                height: image.height
	            },
	            resizeRatio: {
	                width: imageData.width / r,
	                height: imageData.height / r
	            },
	            resizeScale: {
	                width: Math.min(bufferImageWidth / imageData.width),
	                height: Math.min(bufferImageHeight / imageData.height)
	            }
	        }

	        __dataImage[dataType] = dataImage

	        widthContent = $(boxText[idx]).width();
	        heightContent = $(boxText[idx]).height();

	        centerX = image.width / 2;
	        centerY = image.height / 2;

	        __centerX[dataType] = centerX;
	        __centerY[dataType] = centerY;

	        xLineCenter = centerX - (widthContent / 2);
	        yLineCenter = centerY - (heightContent / 2);

	        var marginRange = image.width * (2 / 100);
	        yRange = {
	            min: centerY - marginRange,
	            max: centerY + marginRange
	        }
	        __yRange[dataType] = yRange

            $('#data-'+dataType).val(JSON.stringify(dataImage))
            $('.data-image').val(JSON.stringify(dataImage))
	    }else{
            console.log('else');
	    }
    }

    // console.log(__boxData)
    // console.log(boxText)

    // console.log($(`#${imageData.target}`).parent())
    // widthContent = $('#MainName').width();
    // heightContent = $('#MainName').height();


    // centerX = image.clientWidth / 2;
    // centerY = image.clientHeight / 2;

    // var xLineCenter = centerX - (widthContent / 2);
    // var yLineCenter = centerY - (heightContent / 2);

    // let dataImage = {
    //     original: {
    //         width: imageData.widthContent,
    //         height: imageData.height
    //     },
    //     originalRatio: {
    //         w: imageData.ratioWidth,
    //         h: imageData.ratioHeight
    //     },
    //     resizeTransform: {
    //         width: 0,
    //         height: 0
    //     },
    //     resize: false,
    //     resizeScale: {
    //         width: 0,
    //         height: 0
    //     }
    // }

    // let dataNomor = ['PREI', 'SEM', 'ERP', 'VI', 2020, '0001'];

    // if(imageData.width > 700){
    //     let bufferImageWidth = image.clientWidth;
    //     let bufferImageHeight = image.clientHeight;
    //     $('img').width(700)

    //     image = document.getElementById('produkImg');

    //     r = gcd(image.clientWidth, image.clientHeight)

    //     dataImage = {
    //         ...dataImage,
    //         resize: true,
    //         original: {
    //             width: bufferImageWidth,
    //             height: bufferImageHeight
    //         },
    //         resizeTransform: {
    //             width: image.clientWidth,
    //             height: image.clientHeight
    //         },
    //         resizeRatio: {
    //             width: image.clientWidth / r,
    //             height: image.clientHeight / r
    //         },
    //         resizeScale: {
    //             width: Math.min(bufferImageWidth / image.clientWidth),
    //             height: Math.min(bufferImageHeight / image.clientHeight)
    //         }
    //     }

    //     $('#data-image').val(JSON.stringify(dataImage));

    //     // console.log(dataImage.original.width / dataImage.transform.width, dataImage.original.height / dataImage.transform.height)

    //     widthContent = $('#MainName').width();
    //     heightContent = $('#MainName').height();

    //     centerX = image.clientWidth / 2;
    //     centerY = image.clientHeight / 2;
    //     xLineCenter = centerX - (widthContent / 2);
    //     yLineCenter = centerY - (heightContent / 2);

    //     var marginRange = image.clientWidth * (2 / 100);
    //     yRange = {
    //         min: centerY - marginRange,
    //         max: centerY + marginRange
    //     }

    //     let idSvg = 'svgLineY';
    //     var svg = $(`#${idSvg}`);
    //     var h = svg.attr('height').replace('px', image.heightContent);
    //     var w = svg.attr('width').replace('px', image.widthContent);

    //     svg.attr({
    //         width: image.clientWidth,
    //         height: image.clientHeight,
    //     })
    //     let rect = svg.find('rect')

    //     let X = $(rect[0])
    //     let Y = $(rect[1]);

    //     X.attr('x', centerX)
    //     Y.attr('y', centerY)

    //     console.log(image.clientWidth)

    //     $('#parentCanvas').css('width', image.clientWidth);
    //     // $('#MainName').css({top: centerY - (heightContent / 2), left: centerX - (widthContent / 2)})
    // }
}

function setupPlacement(target, placement){
    let ini = $('#'+target);
    let color = '#000';
    if(placement.color){
        color = placement.color
    }
    placement.color = color;
    ini.css({'font-size': placement.fontSize, 'left': placement.left, 'top': placement.top, 'color': color})
    let idDataType = ini.attr('data-type')
    $('#data-'+idDataType).val(JSON.stringify(placement))
}


function updateData(width, height){
    widthContent = width;
    heightContent = height;
    xLineCenter = centerX - (widthContent / 2);
    yLineCenter = centerY - (heightContent / 2);
}

function moveWithArror(type, value, minValue, maxValue){
    let valueData = $(`#${ClickID}`).css(type).substr()
    valueData = Number(valueData.substr(0, valueData.length - 2));
    value = valueData + value;
    $(`#${ClickID}`).css(type, value)
    if(type === 'top'){
        if(value < minValue){
            $(`#${ClickID}`).css(type, minValue)
        }
        if(value + (heightContent) > maxValue){
            $(`#${ClickID}`).css(type, maxValue - (heightContent))
        }
    }else{
        if(value < minValue){
            $(`#${ClickID}`).css(type, minValue)
        }
        if(value + (widthContent) > maxValue){
            $(`#${ClickID}`).css(type, maxValue - (widthContent))
        }
    }
}