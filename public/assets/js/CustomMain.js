var imageData = {
    width: 0,
    height: 0,
    ratioWidth: 0,
    ratioHeight: 0
}

function gcd (a, b) {
    return (b == 0) ? a : gcd (b, a%b);
}

function getImageWidthandHeight(){
    return imageData;
}
function readURL(input, target, ratioWidth = 0, ratioHeight = 0) {
    return new Promise(async resolve => {
        if (input.files && input.files[0]) {
            // var file = document.querySelector('input[type=file]').files[0];
            filename = $(input).val().split("\\");
            filename = filename[filename.length - 1];
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image;
                img.onload = function() {
                    const { width, height } = img;
                    var w = width;
                    var h = height;
                    var r = gcd (w, h);

                    imageData.width = width;
                    imageData.height = height;
                    imageData.image = this;
                    imageData.input = input;
                    imageData.target = target;

                    let rw = w/r;
                    let rh = h/r;
                    if(ratioWidth === 0 && ratioHeight === 0){
                        imageData.ratioWidth = rw;
                        imageData.ratioHeight = rh;
                        // imageData.this = this;
                        $(`#${target}`).attr('src', e.target.result);
                        $(`.${target}`).html(filename);
                        resolve(imageData)
                    }else{
                        if(rw !== ratioWidth && rh !== ratioHeight){
                            swal("Gagal", `Gambar yang dipilih rasionya bukan ${ratioWidth}:${ratioHeight}`, "error");
                            $(input).val(null);
                            $(`.${target}`).html('Pilih Gambar Kategori');
                            $(`#${target}`).attr('src', "")
                        }else{
                            $(`#${target}`).attr('src', e.target.result);
                            $(`.${target}`).html(filename);
                            imageData.ratioWidth = rw;
                            imageData.ratioHeight = rh;
                            // imageData.this = this;
                        }
                        resolve(imageData)
                    }
                };

                img.src = reader.result;
            }
            reader.readAsDataURL(input.files[0]);
        }

    })
}