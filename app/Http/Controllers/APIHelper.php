<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Session;

use Illuminate\Support\Facades\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class APIHelper extends Controller
{
    private function objData(){
        $data = (object)array();
        $data->state = false;
        $data->msg = "Failed";
        $data->code = 105;
        $data->http = 0;
        $data->data = [];
        return $data;
    }

    public function POST($data){
        try{
            $result = Http::withHeaders([
                'Content-Type' => 'application/json'
            ])->post($data['url'], $data['form']);
            return $result->json();
        }catch(\Exception $err){
            $error = $this->objData();
            $error->msg = "Failed to Fetch Data".var_export(json_encode($err));
            $error->http = $err->getCode();
            return $error;
        }
    }

    public function GETAUTH($data){
        try{
            $result = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->withToken(Session::get('token'))->get($data['url']);

            return $result->json();
        }catch(\Exception $err){
            $error = $this->objData();
            $error->msg = "Failed to Fetch Data".var_export(json_encode($err));
            $error->http = $err->getCode();
            return $error;
        }
    }

    public function POSTIMAGEAUTH($data){
        try{
            $result = Http::attach($data['image']['name'], $data['image']['content'], $data['image']['filename'])
            ->withHeaders([
                'Authorization' => 'Bearer '.Session::get('token')
            ])->post($data['url'], $data['form']);
            return $result->json();
        }catch(\Exception $err){
            $error = $this->objData();
            $error->msg = "Failed to Fetch Data".var_export(json_encode($err));
            $error->http = $err->getCode();
            return $error;
        }
    }

    public function POSTAUTH($data){
        try{
            $result = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->withToken(Session::get('token'))->post($data['url'], $data['form']);
            return $result->json();
        }catch(\Exception $err){
            $error = $this->objData();
            $error->msg = "Failed to Request API".var_export(json_encode($err));
            $error->http = $err->getCode();
            return $error;
        }
    }
    public function POSTIMAGEAUTHGUZZLE($data){
        $client = new Client();
        try{
            $result = $client->post($data['url'], [
                'headers' => [
                    'Authorization' => 'Bearer '.Session::get('token'),
                ],
                'multipart' => $data['multipart']
            ]);
            $response = $result->getBody()->getContents();
            $decode = json_decode($response, true);
            if(isset($decode->error)){
                throw $err;
            }
            return $decode;
        }catch(\Exception $err){
            $error = $this->objData();
            $error->msg = "Failed to Fetch Data".var_export(json_encode($err));
            $error->http = $err->getCode();
            return $error;
        }
    }

    public function postSendAuth($data){
        $client = new Client();
        $data['token'] = 'Bearer '.$data['token'];
        try{
            $result = $client->post($data['url'], [
                'headers' => [
                    'Authorization' => $data['token']
                ],
                'form_params' => $data['body']
            ]);
            $response = $result->getBody()->getContents();
            $decode = json_decode($response);
            if(isset($decode->error)){
                throw $err;
            }
            return $decode;
        }catch(\Exception $err){
            $error = $this->objData();
            $error->msg = "Failed to Fetch Data".var_export(json_encode($err));
            $error->http = $err->getCode();
            return $error;
        }
    }
}
