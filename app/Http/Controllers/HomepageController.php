<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;

use Session;
use ENV;

class HomepageController extends Controller
{
	public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    public function Index(){
    	$data['url'] = ENV::link('Util/HomepageStatistic');
        $data['form'] = Array('id' => Session::get('id'));

        $response = $this->api->POSTAUTH($data);

        if($response['state']){
        	return view('Dashboard.Dashboard', ['data' => $response['data']]);
        }else{
        	return redirect()->route('LogoutProcess')->withErrors(['error' => $response['message'],'code' => $response['code']]);
        }

    }
}
