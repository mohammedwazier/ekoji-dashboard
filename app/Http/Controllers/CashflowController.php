<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;
use Session;
use ENV;

class CashflowController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    public function getListUser(){
        $data['url'] = ENV::link('Cashflow/allCashflow');
        $data['form'] = Array('id' => Session::get('id'));
        $response = (array)$this->api->POSTAUTH($data);
        return $response;
    }

    public function Index(){
        $response = $this->getListUser();
        if($response['state']){
            $data = $response['data'];
            return view('Dashboard.Cashflow', compact('data'));
        }else{
            return redirect()->route('Dashboard');
        }
    }

    public function DetailPopup($id){
        $response = $this->getListUser();
        if($response['state']){
            $data = $response['data'];
            return view('Dashboard.Cashflow', compact('data'))->withErrors(['popup' => true, 'id' => $id]);
        }else{
            return redirect()->route('Dashboard');
        }
    }

    public function Detail(Request $req){
        $data['url'] = ENV::link('Cashflow/singleCashflow');
        $data['form'] = Array('id' => Session::get('id'), 'refid' => $req->refid);
        $response = $this->api->POSTAUTH($data);
        $res['cashflow'] = $response['data'];

        return view('Popup.CashflowPopup', ['cashflow' => $response['data']]);
    }
}
