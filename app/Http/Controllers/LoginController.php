<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\APIHelper;
use App\Http\Controllers\MainController;

use Session;
use Carbon\Carbon;
use ENV;


class LoginController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }
    function Index(){
        $state = $this->controller->VerifyState();
        if(!$state){
            return view('login');
        }
        return redirect()->Route('Dashboard');
    }

    function Login(Request $req){
        $data = Array('input' => $req->username, 'password' => $req->password, 'tipe' => 'dashboard', 'group' => 'prexux');
        $link = ENV::link('Login/Login');
        $new['form'] = $data;
        $new['url'] = $link;
        $response = $this->api->POST($new);
        $data = (object)$response['data'];
        if($response['state']){
            Session::put('token', $data->token);
            Session::put('id', $data->id);
            Session::put('username', $data->username);
            Session::put('data', $data);
            Session::put('role', $data->role);
            Session::put('group', $data->group);
            Session::put('loggedin', Carbon::now('Asia/Jakarta'));
            return redirect()->route('Dashboard');
        }else{
            return redirect()->back()->withErrors(['error' => $response['message'],'code' => $response['code']]);
        }
    }

    function Logout(){
        $link = ENV::link('Login/Logout');
        $form = Array('username' => Session::get('username'), 'id' => Session::get('id'));
        $data['form'] = $form;
        $data['url'] = $link;
        $response = $this->api->POSTAUTH($data);
        Session::flush();
        if($response['state']){
            return redirect()->route('Login');
        }else{
            return redirect()->back()->withErrors(['error' => $response['message'], 'code' => $response['code']]);
        }
    }
}
