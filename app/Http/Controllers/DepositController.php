<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;

use Session;
use ENV;

class DepositController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    public function Index(Request $req){
        $statistic['url'] = ENV::link('Payment/statisticDeposit');
        $statistic['form'] = Array('id' => Session::get('id'));

        $data['url'] = ENV::link('Payment/getAllDeposit');
        $data['form'] = Array('id' => Session::get('id'));

        $mutasi['url'] = ENV::link('Payment/allMutasiExternal');
        $mutasi['form'] = Array('id' => Session::get('id'));
        $responseMutasi = $this->api->POSTAUTH($mutasi);
        $response = $this->api->POSTAUTH($data);
        $responseStatistic = $this->api->POSTAUTH($statistic);
        if($response['state']){
            $ret['data'] = $response['data'];
            $ret['mutasi'] = $responseMutasi['data'];
            $ret['statistic'] = $responseStatistic['data'];
            return view('Dashboard.Deposit', compact('ret'));
        }else{
            return redirect()->route('Dashboard');
        }
    }

    public function Search(Request $req){
        $data['url'] = ENV::link('Payment/depositSearch');
        $data['form'] = Array('id' => Session::get('id'), 'value' => $req->search);

        $statistic['url'] = ENV::link('Payment/statisticDeposit');
        $statistic['form'] = Array('id' => Session::get('id'));

        // $mutasi['url'] = ENV::link('Payment/allMutasiExternal');
        // $mutasi['form'] = Array('id' => Session::get('id'));

        // $responseMutasi = $this->api->POSTAUTH($mutasi);
        $response = $this->api->POSTAUTH($data);
        $responseStatistic = $this->api->POSTAUTH($statistic);
        if($response['state']){
            $ret['data'] = $response['data']['deposit'];
            $ret['mutasi'] = $response['data']['mutasi_external'];
            $ret['statistic'] = $responseStatistic['data'];

            return view('Dashboard.Deposit', compact('ret'))->withErrors(['search' => true, 'value' => $req->search, 'message' => $response['message']]);
        }else{
            return redirect()->route('Dashboard');
        }
    }

    public function DetailDeposit(Request $req, $id){
        $data['url'] = ENV::link('Payment/detailDeposit');
        $data['form'] = Array('id' => $id);
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            $data = $response['data'];
            return view('Dashboard.DepositDetail', compact('data'));
        }else{
            return redirect()->route('Dashboard');
        }
    }

    public function UpdateDeposit(Request $req){
        $data['url'] = ENV::link('Payment/processDeposit/force');
        $status = $req->status === "on" ? 2 : $req->status;

        $data['form'] = Array('id' => $req->id, 'status' => $req->status, 'adminid' => Session::get('id'));
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            return redirect()->route('Deposit')->withErrors(['success' => 'Success Update Deposit Transaction']);
        }else{
            return redirect()->back()->withErrors(['failed' => 'Failed to Update Deposit Transaction']);
        }
    }

    public function wrongTrfTrxDeposit(Request $req){
        $data['url'] = ENV::link('Payment/processDeposit');

        $data['form'] = Array('id' => $req->id, 'status' => $req->status, 'adminid' => Session::get('id'));
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            return redirect()->route('Deposit')->withErrors(['success' => 'Success Update Deposit Transaction']);
        }else{
            return redirect()->back()->withErrors(['failed' => 'Failed to Update Deposit Transaction']);
        }
    }

    public function DetailMutasiExternal(Request $req){
        $data['url'] = ENV::link('Payment/getDepositProblem');
        $data['form'] = Array('id' => Session::get('id'));
        $response = $this->api->POSTAUTH($data);
        $ret['data'] = $req->all();
        $ret['deposit'] = $response['data'];
        return view('Popup.DepositMutasiPopup', compact('ret'));
    }

    public function UpdateDepositTransaksi(Request $req){
        $data['url'] = ENV::link('Payment/changeDepositStatusProblem');
        $data['form'] = array('id' => Session::get('id'), 'status_deposit' => $req->status_deposit, 'service_id' => $req->service_id, 'data_trx' => $req->data_trx);

        $response = $this->api->POSTAUTH($data);
        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }
}
