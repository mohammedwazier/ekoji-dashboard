<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\MainController;

use Session;
use Carbon\Carbon;
use ENV;

class OTPController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    function Index(){
        $data['url'] = ENV::link('Product/getOTPList');
        $data['form'] = Array('id' => Session::get('id'));
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            $data = $response['data'];
            return view('Dashboard.OTP', compact('data'));
        }else{
            return redirect()->route('Dashboard');
        }
    }
}
