<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;

use Session;
use ENV;

class SettingController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    public function Index(Request $req){
    	$data['url'] = ENV::link('Util/GetSetting');
        $data['form'] = Array('id' => Session::get('id'));

        $response = $this->api->POSTAUTH($data);
        if($response['state']){
        	return view('Dashboard.Setting', ['data' => $response['data']]);
        }else{
        	return redirect()->back()->withErrors(['state' => $response['state'], 'message' => $response['message']]);
        }
    }

    public function CreateSetting(Request $req){
    	$data['url'] = ENV::link('Util/CreateSetting');
        $data['form'] = Array(
        	'id' => Session::get('id'),
        	'kode' => strtolower($req->kode),
        	'keterangan' => $req->keterangan,
        	'value' => $req->value,
        	'typevalue' => $req->typevalue,
        	'active' => $req->active === "on" ? 1 : 0
        );
        $response = $this->api->POSTAUTH($data);
        return redirect()->back()->withErrors(['state' => $response['state'], 'message' => $response['message']]);
    }

    public function DetailPopup(Request $req){
    	$data['url'] = ENV::link('Util/SingleSetting');
        $data['form'] = Array('id' => Session::get('id'), 'kode' => $req->kode);

        $response = $this->api->POSTAUTH($data);
        return view('Popup.SettingPopup', ['data' => $response['data']]);
    }

    public function Update(Request $req){
    	$data['url'] = ENV::link('Util/UpdateSetting');
        $data['form'] = Array(
        	'idsetting' => $req->id,
        	'id' => Session::get('id'),
        	'kode' => strtolower($req->kode),
        	'keterangan' => $req->keterangan,
        	'value' => $req->value,
        	'typevalue' => $req->typevalue,
        	'active' => $req->active === "on" ? 1 : 0
        );
        $response = $this->api->POSTAUTH($data);
        return redirect()->route('Setting')->withErrors(['state' => $response['state'], 'message' => $response['message']]);
    }

    public function DeleteSetting(Request $req, $kode){
        dd($kode);
    }
}
