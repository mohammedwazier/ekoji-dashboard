<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Multipart extends Controller
{
    public function multipartSend($arrData){
        $pushed = [];
        foreach ($arrData as $values) {
          $ar = array(
            'Content-type' => 'multipart/form-data',
            'name' => $values['name'],
            'contents' => $values['contents']
          );
          array_push($pushed, $ar);
        }
        return $pushed;
      }
}
