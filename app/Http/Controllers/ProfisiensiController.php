<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;

use Session;
use ENV;

class ProfisiensiController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    public function Index(Request $req){
    	$data['url'] = ENV::link('Product/getProfisiensiUser');
        $data['form'] = Array('id' => Session::get('id'), 'value' => $req->search);

        $response = $this->api->POSTAUTH($data);
        return view('Dashboard.Profisiensi', ['data' => $response['data']]);
    }

    public function Create(Request $req){
    	$data['url'] = ENV::link('Product/createUserProfisiensi');
        $data['form'] = Array(
        	'id' => Session::get('id'),
        	'username' => $req->prf_username,
        	'password' => $req->prf_password,
        	'refid' => $req->trx_refid,
        	'profileid' => $req->userid
        );
        $response = $this->api->POSTAUTH($data);
        return $response;
    }

    public function Search(Request $req){
        $data['url'] = ENV::link('Product/searchProfisiensi');
        $data['form'] = Array('id' => Session::get('id'), 'search' => $req->search);
        $response = (array)$this->api->POSTAUTH($data);
        if($response['state']){
            $user = [];
            $user['user'] = $response['data'];
            return view('Dashboard.Profisiensi', ['data' => $response['data'], 'response' => array('start' => 0)])->withErrors(['search' => true, 'value' => $req->search, 'message' => $response['message']]);
        }else{
            return redirect()->back();
        }
    }
}
