<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;
use App;
use ENV;
use Session;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class EventController extends Controller
{
	public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    public function getData($page = 1){
        $data['url'] = ENV::link("Event/allEvent?page={$page}");
        $data['form'] = Array('id' => Session::get('id'));
        $response = $this->api->POSTAUTH($data);
        return $response;
    }

    public function Index(Request $req){
        $page = $this->controller->validatePage($req);
    	$response = $this->getData($page);

        if($response['state']){
            $data = $this->controller->paginateData($response['data']);
    		return view('Dashboard.Event', ['data' => $data, 'response' => $response['data']]);
        }else{
        	return redirect()->route('Dashboard');
        }
    }

    public function Search(Request $req){
        $page = $this->controller->validatePage($req);
        $data['url'] = ENV::link("Event/searchEvent?page={$page}");
        $data['form'] = Array('id' => Session::get('id'), 'search' => $req->search);
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            $data = $this->controller->paginateData($response['data']);
            return view('Dashboard.Event', ['data' => $data, 'response' => $response['data']])->withErrors(
                [
                    'search' => true,
                    'value' => $req->search,
                    'message' => $response['message'],
                    'state' => true
                ]);
        }else{
            return redirect()->route('Dashboard');
        }
        // dd($response);
        // return $response;
    }

    public function Create(Request $req){
        $file[] = $req->file('cover');

        $multi = array(
            array('name' => 'nilai', 'contents' => $req->nilai),
            array('name' => 'name', 'contents' => $req->name),
            array('name' => 'description', 'contents' => $req->description),
            array('name' => 'event_type', 'contents' => $req->event_type),
            array('name' => 'startdate', 'contents' => $req->startdate),
            array('name' => 'enddate', 'contents' => $req->enddate),
            array('name' => 'starthour', 'contents' => $req->starthour),
            array('name' => 'endhour', 'contents' => $req->endhour),
            array('name' => 'method', 'contents' => $req->method),
            array('name' => 'id', 'contents' => Session::get('id')),
            array('name' => 'used', 'contents' => $req->used),
            array('name' => 'pay_method', 'contents' => $req->pay_method)
        );
        $d = [];
        foreach ($req->except(['_token', 'name', 'description', 'event_type', 'method', 'startdate', 'enddate', 'starthour', 'endhour', 'nilai', 'used', 'nilai', 'pay_method']) as $key => $part) {
            array_push($d, $key);
        }

        if(count($file) > 0){
            for($idx = 0; $idx < count($file); $idx++){
                $file[$idx]->move(public_path('Upload'), $file[$idx]->getClientOriginalName());
                $fileUpload = "Upload/{$file[$idx]->getClientOriginalName()}";
                $open_image = fopen($fileUpload, 'r');
                $photoArray = array('name' => $d[$idx], 'contents' => $open_image);
                array_push($multi, $photoArray);
            }
        }

        $multipart = new Multipart();
        $data['multipart'] = $multipart->multipartSend($multi);
        $data['url'] = ENV::link('Event/createEvent');

        $response = (array)$this->api->POSTIMAGEAUTHGUZZLE($data);
        if($response['state']){
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => true ]);
        }else{
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false ]);
        }
    }

    public function PopupEvent(Request $req){
        $data['url'] = ENV::link("Event/singleEvent");
        $data['form'] = Array('id' => Session::get('id'), 'event_id' => $req->eventid);
        $response = $this->api->POSTAUTH($data);
        return view('Popup.EventPopup', ['data' => $response['data'], 'state' => $response['state']]);
    }

    public function Delete(Request $req, $id){
        $data['url'] = ENV::link("Event/softDelete");
        $data['form'] = Array('id' => Session::get('id'), 'eventid' => $id);
        $response = (array)$this->api->POSTAUTH($data);
        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }

    public function Update(Request $req){
        $file = [];
        if($req->hasFile('cover')){
            $file[] = $req->file('cover');
        }

        $multi = array(
            array('name' => 'nilai', 'contents' => $req->nilai),
            array('name' => 'name', 'contents' => $req->name),
            array('name' => 'description', 'contents' => $req->description),
            array('name' => 'event_type', 'contents' => $req->event_type),
            array('name' => 'startdate', 'contents' => $req->startdate),
            array('name' => 'enddate', 'contents' => $req->enddate),
            array('name' => 'starthour', 'contents' => $req->starthour),
            array('name' => 'endhour', 'contents' => $req->endhour),
            array('name' => 'method', 'contents' => $req->method),
            array('name' => 'id', 'contents' => Session::get('id')),
            array('name' => 'used', 'contents' => $req->used),
            array('name' => 'pay_method', 'contents' => $req->pay_method),
            array('name' => 'eventid', 'contents' => $req->eventid),
            array('name' => 'rawdata', 'contents' => $req->raw),
        );
        $d = [];
        foreach ($req->except(['_token', 'name', 'description', 'event_type', 'method', 'startdate', 'enddate', 'starthour', 'endhour', 'nilai', 'used', 'nilai', 'pay_method']) as $key => $part) {
            array_push($d, $key);
        }


        if(count($file) > 0){
            for($idx = 0; $idx < count($file); $idx++){
                $file[$idx]->move(public_path('Upload'), $file[$idx]->getClientOriginalName());
                $fileUpload = "Upload/{$file[$idx]->getClientOriginalName()}";
                $open_image = fopen($fileUpload, 'r');
                $photoArray = array('name' => $d[$idx], 'contents' => $open_image);
                array_push($multi, $photoArray);
            }
        }

        $multipart = new Multipart();
        $data['multipart'] = $multipart->multipartSend($multi);
        $data['url'] = ENV::link('Event/updateEvent');
        $response = (array)$this->api->POSTIMAGEAUTHGUZZLE($data);
        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);

    }
}
