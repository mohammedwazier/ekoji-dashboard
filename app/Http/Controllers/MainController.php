<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use ENV;
use Carbon\Carbon;

use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class MainController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
    }

    public function paginateData($response){
        $data = new Paginator(
                $response['list'],
                $response['total'],
                $response['per_page'],
                $response['current_page'],
                [
                    'path' => Paginator::resolveCurrentPath(),
                    'query' => array('page' => $response['current_page'])
            ]);

        return $data;
    }

    public function CustomPaginate($response, $page){
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;
        $data = array(
            'list' => array_slice($response, $offset, $perPage, true),
            'start' => $offset,
            'per_page' => $perPage,
            'total' => count($response),
            'current_page' => $page
        );

        $data = $this->paginateData($data);
        return $data;
    }

    public function validatePage(Request $req){
        $page = !empty($req->page) ? (int)$req->page : 1;
        return $page;
    }

    public function Verify(){
        if(!Session::get('loggedin')){
            return redirect()->route('Login');
        }else{
            return redirect()->route('Dashboard');
        }
    }

    public function VerifyState(){
        if(!Session::get('loggedin')){
            return false;
        }else{
            return true;
        }
    }

    public static function statusEnabled($status){
        $status = (int)$status;
        switch($status){
            case 0 : {
                return 'Disabled';
            } break;
            case 1 : {
                return 'Enabled';
            } break;
        }
    }

    public static function UserStatus($status){
        $status = (int)$status;
        switch($status){
            case 0 : {
                return 'Suspend';
            } break;
            case 1 : {
                return 'Active';
            } break;
            case 2 : {
                return 'Deleted';
            } break;
        }
    }

    public static function ValidateDataNumber($data){
        try{
            $data = (int)$data;
            $data = "+".$data;
            return $data;
        }catch(Exception $err){
            return $err;
        }
    }

    public static function StatusIsActive($status){
        switch($status){
            case 1: {
                echo 'Active';
            } break;
            case 2: {
                echo 'Deleted';
            }
        }
    }

    public static function romanize($num) {
        $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'XC' => 90, 'L' => 50, 'XL' => 50, 'X' => 10, 'V' => '5', 'IV' => 4, 'I' => 1);
        $roman = '';
        foreach ($lookup as $key => $value) {
            while ( $num >= $value ) {
                $roman = $roman.$key;
                $num -= $value;
            }
        }
        return $roman;
    }

    public static function statusDeposit($status){
        switch($status){
            case 0: {
                echo 'Waiting To Transfer';
            } break;
            case 1: {
                echo 'Sudah Transfer';
            } break;
            case 2: {
                echo 'Proses Cron';
            } break;
            case 3: {
                echo 'Expired';
            } break;
            case 4 : {
                echo 'Done';
            } break;
            case 5 : {
                echo 'Failed Deposit';
            } break;
            case 6: {
                echo 'Declined';
            } break;
            case 7: {
                echo 'Force Topup';
            }
        }
    }

    public static function statusMutasi($status){
        switch($status){
            case 0: {
                echo 'Not Valid Transfer';
            } break;
            case 1: {
                echo 'Mutasi Proses';
            } break;
            case 2: {
                echo 'Done Process';
            } break;
        }
    }

    public static function compareStatusProcess($ref){
        switch ($ref) {
            case 'Q':{
                echo 'Pending, Dalam antrian Buffer';
            } break;
            case 'P':{
                echo 'Transaksi sedang dalam Proses';
            } break;
            case 'S':{
                echo 'Sukses, Transaksi Sukses';
            } break;
            default: {
            } break;
        }
    }

    public static function checkStatusDeposit($status){
        if($status === 1){
            return true;
        }else{
            return false;
        }
    }

    public static function getNotifDeposit(){
        $api = new APIHelper();
        $data['form'] = array('id' => Session::get('id'));
        $data['url'] = ENV::link('Payment/depositNotif');
        $response = $api->POSTAUTH($data);
        return $response['data'];
    }

    public static function rupiah($data){
        return 'Rp.'.number_format($data,0,',-','.');
    }

    public static function hiddenNumber($number){
        $number = substr($number, 8);
        $number = '********'.$number;
        return $number;
    }
}
