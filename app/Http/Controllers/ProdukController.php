<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;
use App;
use ENV;
use Session;
use Carbon\Carbon;

ini_set('memory_limit', '512M');
ini_set('post_max_size', '512M');
class ProdukController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }
    function Index(Request $req){
        try{
            $page = $this->controller->validatePage($req);
            $static['url'] = ENV::link('Product/statistic');
            $static['form'] = Array('id' => Session::get('id'));
            $responseStatic = $this->api->POSTAUTH($static);

            $category['url'] = ENV::link('Product/getAllCategory');
            $category['form'] = Array('id' => Session::get('id'));
            $responseCategory = $this->api->POSTAUTH($category);

            $product['url'] = ENV::link('Product/getAllProductMini');
            $product['form'] = Array('id' => Session::get('id'));
            $responseproduct = $this->api->POSTAUTH($product);

            $dataPaginate = $this->controller->CustomPaginate($responseproduct['data'], $page);

            $user['url'] = ENV::link('Profile/selectRoleUser');
            $user['form'] = Array('id' => Session::get('id'), 'role' => 'pemateri');
            $responseUser = $this->api->POSTAUTH($user);

            $data['kategori'] = $responseCategory['data'];
            // $data['produk'] =  $responseproduct['data'];
            $data['static'] = $responseStatic['data'];
            $data['user'] = $responseUser['data'];
            $data['produk_paginate'] = $dataPaginate;
            $data['current_page'] = $page;
            $data['total'] = count($responseproduct['data']);
            return view('Dashboard.Produk', compact('data'));
        }catch(\Exception $err){
            return redirect()->route('Dashboard')->withErrors(['state' => true, 'message' => 'Error when loading Page Produk', 'error' => var_export($err)]);
        }
    }

    public function PopupProduk(Request $req, $id){
        try{
            $static['url'] = ENV::link('Product/statistic');
            $static['form'] = Array('id' => Session::get('id'));
            $responseStatic = $this->api->POSTAUTH($static);

            $category['url'] = ENV::link('Product/getAllCategory');
            $category['form'] = Array('id' => Session::get('id'));
            $responseCategory = $this->api->POSTAUTH($category);

            $product['url'] = ENV::link('Product/getAllProductMini');
            $product['form'] = Array('id' => Session::get('id'));
            $responseproduct = $this->api->POSTAUTH($product);
            // dd($responseproduct);

            $user['url'] = ENV::link('Profile/allPemateri');
            $user['form'] = Array('id' => Session::get('id'));
            $responseUser = $this->api->POSTAUTH($user);

            $data['kategori'] = $responseCategory['data'];
            $data['produk'] =  $responseproduct['data'];
            $data['static'] = $responseStatic['data'];
            $data['user'] = $responseUser['data'];
            $data['popupData'] = true;
            $data['id'] = $id;

            return view('Dashboard.Produk', compact('data'));
        }catch(\Exception $err){
            return redirect()->route('Dashboard');
        }
    }

    public function GetSingleKategori(Request $req, $id){
        $data['url'] = ENV::link('Product/getSingleCategory');
        $data['form'] = Array('groupid' => $id);
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            $data = $response['data'];
            return view('Dashboard.ProdukKategoriDetail', compact('data'));
        }else{
            return redirect()->back();
        }
    }

    public function UpdateKategori(Request $req){
        $file = $req->file('image');
        $data['url'] = ENV::link('Product/updateCategory');

        $multi = array(
            array('name' => 'nama', 'contents' => $req->name),
            array('name' => 'id', 'contents' => $req->id),
        );

        if($file){

            $file->move(public_path('Upload'), $file->getClientOriginalName());
            $fileUpload = "Upload/{$file->getClientOriginalName()}";

            $open_image = fopen($fileUpload, 'r');
            $photoArray = array('name' => 'image', 'contents' => $open_image);
            array_push($multi, $photoArray);
        }

        $multipart = new Multipart();
        $data['multipart'] = $multipart->multipartSend($multi);
        $response = $this->api->POSTIMAGEAUTHGUZZLE($data);
        if($response['state']){
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }else{
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }
    }

    function DeleteKategori(Request $req, $id){
        $data['form'] = array('id' => $id);
        $data['url'] = ENV::link('Product/deleteCategory');
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }else{
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }
    }

    function CreateKategori(Request $req){
        $file = $req->file('image');
        $file->move(public_path('Upload'), $file->getClientOriginalName());
        $fileUpload = "Upload/{$file->getClientOriginalName()}";

        $open_image = fopen($fileUpload, 'r');
        $image = array('name' => 'photo', 'content' => $open_image, 'filename' => $file->getClientOriginalName());
        $data['image'] = $image;
        $data['form'] = array('nama' => $req->name);
        $data['url'] = ENV::link('Product/createCategory');
        $response = $this->api->POSTIMAGEAUTH($data);
        if($response['state']){
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }else{
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }
    }

    public function CreateProduk(Request $req){
        try{
            $file = array();
            if($req->hasFile('certificate')){
                $file[] = $req->file('certificate');
            }
    
            $file[] = $req->file('cover');
    
            $imgData = json_decode($req->data_image, true);
            $namaData = json_decode($req->data_name, true);
            $nomorData = json_decode($req->data_nomor, true);
    
            $imageSource = array('image' => $imgData, 'nama' => $namaData, 'nomor' => $nomorData);
    
            $d = [];
            foreach ($req->except(['_token', 'name', 'keterangan', 'link', 'harga', 'tipeproduk', 'pemateri', 'startDate', 'endDate', 'data_image', 'data_name', 'data_nomor', 'startTime', 'endTime']) as $key => $part) {
                array_push($d, $key);
            }
    
            $startDate = $req->startDate ? Carbon::create($req->startDate) : Carbon::now();
            $endDate = $req->endDate ? Carbon::create($req->endDate) : Carbon::now();
    
            $startTime = $req->startTime ? Carbon::create($req->startTime)->format('h:i'): Carbon::now('Asia/Jakarta')->format('h:i');
            $endTime = $req->endTime ? Carbon::create($req->endTime)->format('h:i'): Carbon::now('Asia/Jakarta')->format('h:i');
    
            $multi = array(
                array('name' => 'nama', 'contents' => $req->name),
                array('name' => 'keterangan', 'contents' => $req->keterangan),
                array('name' => 'link', 'contents' => $req->link),
                array('name' => 'harga', 'contents' => $req->harga),
                array('name' => 'tipe', 'contents' => $req->tipeproduk),
                array('name' => 'id', 'contents' => Session::get('id')),
                array('name' => 'pemateri', 'contents' => $req->pemateri),
                array('name' => 'startDate', 'contents' => $startDate),
                array('name' => 'endDate', 'contents' => $endDate),
                array('name' => 'imagePlacement', 'contents' => json_encode($imageSource)),
                array('name' => 'startTime', 'contents' => $startTime),
                array('name' => 'endTime', 'contents' => $endTime)
            );
    
            if(count($file) > 0){
                for($idx = 0; $idx < count($file); $idx++){
                    $file[$idx]->move(public_path('Upload'), $file[$idx]->getClientOriginalName());
                    $fileUpload = "Upload/{$file[$idx]->getClientOriginalName()}";
                    $open_image = fopen($fileUpload, 'r');
                    $photoArray = array('name' => $d[$idx], 'contents' => $open_image);
                    array_push($multi, $photoArray);
                }
            }
    
            $multipart = new Multipart();
            $data['multipart'] = $multipart->multipartSend($multi);
            $data['url'] = ENV::link('Product/createProduct');
    
            $response = (Array)$this->api->POSTIMAGEAUTHGUZZLE($data);
    
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
        }catch(\Exception $err){
            return redirect()->back()->withErrors(['message' => "Error Create Product {$req->name}", 'state' => false]);
        }
    }

    public function GetSingleProduk(Request $req){
        $produk['url'] = ENV::link('Product/getSingleProduct');
        $produk['form'] = Array('id' => Session::get('id'), 'produkid' => $req->produkid);
        $response = $this->api->POSTAUTH($produk);

        $category['url'] = ENV::link('Product/getAllCategory');
        $category['form'] = Array('id' => Session::get('id'));
        $responseCategory = $this->api->POSTAUTH($category);

        $user['url'] = ENV::link('Profile/selectRoleUser');
        $user['form'] = Array('id' => Session::get('id'), 'role' => 'pemateri');
        $responseUser = $this->api->POSTAUTH($user);

        $placement['url'] = ENV::link('Product/getPlacementData');
        $placement['form'] = Array('id' => Session::get('id'), 'idproduk' => $req->produkid);
        $placement = $this->api->POSTAUTH($placement);

        $ret['data'] = $response['data'];
        $ret['category'] = $responseCategory['data'];
        $ret['user'] = $responseUser['data'];
        $ret['placement'] = $placement['data'];

        return view('Popup.ProdukPopup', compact('ret'));
    }

    public function UpdateProduk(Request $req){
        $multi = array();
        $file = array();

        if($req->file('certificate')) $file[] = $req->file('certificate');
        if($req->file('cover')) $file[] = $req->file('cover');

        foreach($req->except('_token', 'certificate', 'cover') as $key => $values){
            $multi[] = array('name' => $key, 'contents' => $values);
        }
        if(count($file) > 0){
            $d = array('certificate', 'cover');
            for($idx = 0; $idx < count($file); $idx++){
                $file[$idx]->move(public_path('Upload'), $file[$idx]->getClientOriginalName());
                $fileUpload = "Upload/{$file[$idx]->getClientOriginalName()}";
                $open_image = fopen($fileUpload, 'r');
                $photoArray = array('name' => $d[$idx], 'contents' => $open_image);
                array_push($multi, $photoArray);
            }
        }

        $multipart = new Multipart();
        $data['multipart'] = $multipart->multipartSend($multi);
        $data['url'] = ENV::link('Product/updateProduct');

        $response = (Array)$this->api->POSTIMAGEAUTHGUZZLE($data);

        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }

    public function DeleteProduk(Request $req, $id){
        $data['form'] = array('id' => $id);
        $data['url'] = ENV::link('Product/deleteProduk');
        $response = $this->api->POSTAUTH($data);
        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }

    public function search(Request $req){
        $page = $this->controller->validatePage($req);

        $static['url'] = ENV::link('Product/statistic');
        $static['form'] = Array('id' => Session::get('id'));
        $responseStatic = $this->api->POSTAUTH($static);

        $user['url'] = ENV::link('Profile/selectRoleUser');
        $user['form'] = Array('id' => Session::get('id'), 'role' => 'pemateri');
        $responseUser = $this->api->POSTAUTH($user);


        $data['url'] = ENV::link('Product/searchProduk');
        $data['form'] = Array('id' => Session::get('id'), 'search' => $req->search);
        $response = $this->api->POSTAUTH($data);
        if($response['state']){
            $data = [];
            $data['static'] = $responseStatic['data'];
            $data['user'] = $responseUser['data'];


            $data['kategori'] = $response['data']['kategori'];
            $dataPaginate = $this->controller->CustomPaginate($response['data']['produk'], $page);
            $data['produk_paginate'] = $dataPaginate;
            $data['current_page'] = $page;
            $data['total'] = count($response['data']['produk']);


            return view('Dashboard.Produk', compact('data'))->withErrors(['search' => true, 'value' => $req->search, 'message' => $response['message'], 'state' => true]);
        }else{
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }
    }

    public function EditPlacementData(Request $req){
        return view('Popup.ProdukPlacementPopup', ['data' => $req->data, 'image' => $req->image, 'id' => $req->id]);
    }

    public function savePlacement(Request $req){
        $imgData = json_decode($req->data_image, true);
        $namaData = json_decode($req->data_name, true);
        $nomorData = json_decode($req->data_nomor, true);

        $imageSource = json_encode(array('image' => $imgData, 'nama' => $namaData, 'nomor' => $nomorData));

        $data['url'] = ENV::link('Product/updatePlacementProduk');
        $data['form'] = Array('id' => Session::get('id'), 'produkid' => $req->id, 'placement' => $imageSource);
        $response = $this->api->POSTAUTH($data);

        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }
}
