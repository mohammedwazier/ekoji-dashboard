<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;
use App;
use ENV;
use Session;
use Carbon\Carbon;

class TransaksiController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }
    public function Index(Request $req){
    	$static['url'] = ENV::link('History/getAllTrx');
        $static['form'] = Array('id' => Session::get('id'));
        $response = $this->api->POSTAUTH($static);

        if($response['state']){
        	$res = $response['data'];
        	return view('Dashboard.Transaksi', compact('res'));
        }else{
        	return redirect()->route('Dashboard');
        }
    }
}
