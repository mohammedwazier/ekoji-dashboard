<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;

use Session;
use ENV;

class UserGuideController extends Controller
{
	public function __construct(){
		$this->api = new APIHelper();
        $this->controller = new MainController();
	}
    public function Index(){
        // dd([Session::all()]);
        $data['url'] = ENV::link('Util/UserguideList');
        $data['form'] = Array('id' => Session::get('id'));

        $response = $this->api->POST($data);
        if($response['state']){
        	return view('Dashboard.Userguide', ['data' => $response['data']]);
        }else{
        	return redirect()->back()->withErrors(['state' => $response['state'], 'message' => $response['message']]);
        }
    }

    public function Create(Request $req){
        // dd($req->all());
    	$file = array();
        if($req->hasFile('file')){
            $file[] = $req->file('file');
        }

        $multi = array(
            array('name' => 'id', 'contents' => Session::get('id')),
            array('name' => 'name', 'contents' => $req->ug_name)
        );

        if(count($file) > 0){
            for($idx = 0; $idx < count($file); $idx++){
                $file[$idx]->move(public_path('Upload'), $file[$idx]->getClientOriginalName());
                $fileUpload = "Upload/{$file[$idx]->getClientOriginalName()}";
                $open_image = fopen($fileUpload, 'r');
                $photoArray = array('name' => 'file', 'contents' => $open_image);
                array_push($multi, $photoArray);
            }
        }

        $multipart = new Multipart();
        $data['multipart'] = $multipart->multipartSend($multi);
        $data['url'] = ENV::link('Util/AddUserguide');

        $response = (Array)$this->api->POSTIMAGEAUTHGUZZLE($data);

        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }

    public function Delete(Request $req, $guideid){
    	$data['url'] = ENV::link('Util/DeleteUserguide');
        $data['form'] = Array('id' => Session::get('id'), 'guideid' => $guideid);

        $response = $this->api->POSTAUTH($data);

        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }
}
