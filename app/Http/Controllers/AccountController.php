<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\APIHelper;
use App\Http\Controllers\Multipart;
use App;
use ENV;
use Session;

class AccountController extends Controller
{
    public function __construct(){
        $this->api = new APIHelper();
        $this->controller = new MainController();
    }

    function getAllUser($page = 1){
        $data['url'] = ENV::link("Profile/all?page={$page}");
        $data['form'] = Array('id' => Session::get('id'));
        $response = $this->api->POSTAUTH($data);
        return $response;
    }

    function Index(Request $req){
        $page = $this->controller->validatePage($req);
        $response = $this->getAllUser($page);
        if($response['state']){
            $data = $this->controller->paginateData($response['data']);
            return view('Dashboard.Profile', ['data' => $data, 'response' => $response['data']]);
        }else{
            return redirect()->route('Dashboard');
        }
    }

    function DetailUser(Request $req, $id){
        $page = $this->controller->validatePage($req);
        $response = $this->getAllUser($page);
        if($response['state']){
            $data = $this->controller->paginateData($response['data']);
            return view('Dashboard.Profile', [
                'data' => $data,
                'response' => $response['data'],
                'findDetail' => true,
                'id' => $id
        ]);
        }else{
            return redirect()->route('Dashboard');
        }
    }

    function CreateUser(Request $req){
        $file = $req->file('image');
        $file->move(public_path('Upload'), $file->getClientOriginalName());
        $fileUpload = "Upload/{$file->getClientOriginalName()}";
        $open_image = fopen($fileUpload, 'r');
        $photoArray = array('name' => 'image', 'contents' => $open_image);

        $multi = [$photoArray];

        $multipart = new Multipart();
        $data['multipart'] = $multipart->multipartSend($multi);
        $data['url'] = ENV::link('Product/upload');

        $response = (Array)$this->api->POSTIMAGEAUTHGUZZLE($data);
        // End upload Image
        $name = $response['data']['fieldData']['image'];
        $data = [];
        $data['url'] = ENV::link('Register/Register/DashboardAdmin');

        $data['form'] = Array(
            'email' => $req->email,
            'nohp' => $req->nohp,
            'nama' => $req->nama,
            'nik' => $req->nik,
            'username' => $req->username,
            'password' => $req->password,
            'tipe' => $req->role,
            'tempatlahir' => $req->tempatlahir,
            'tanggallahir' => $req->tgllahir,
            'alamat' => $req->alamat,
            'gender' => $req->gender,
            'photo' => $name,
            'from' => 'dashboard',
            'group' => 'prexux'
        );
        $response = (array)$this->api->POSTAUTH($data);
        if($response['state']){
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => true]);
        }else{
            return redirect()->back()->withErrors(['message' => $response['message'], 'state' => false]);
        }
    }

    function Search(Request $req){
        $data['url'] = ENV::link('Profile/searchProfile');
        $data['form'] = Array('id' => Session::get('id'), 'search' => $req->search);
        $response = (array)$this->api->POSTAUTH($data);
        if($response['state']){
            $user = [];
            $user['user'] = $response['data'];
            return view('Dashboard.Profile', ['data' => $response['data'], 'response' => array('start' => 0)])->withErrors(['search' => true, 'value' => $req->search, 'message' => $response['message']]);
        }else{
            return redirect()->back();
        }
    }

    public function PopupDetail(Request $req){
        $data['url'] = ENV::link('Profile/detail');
        $data['form'] = Array('id' => Session::get('id'), 'profileid' => $req->profileid);
        $response = (array)$this->api->POSTAUTH($data);

        $data['url'] = ENV::link('History/getAllHistory');
        $data['form'] = Array('id' => $req->profileid);
        $responseHistory = (array)$this->api->POSTAUTH($data);

        $data = [];
        $data['user'] = $response['data'];
        $data['history'] = $responseHistory['data'];
        return view('Popup.ProfilePopup', compact('data'));
    }

    public function Suspend($id){
        $data['url'] = ENV::link('Profile/suspendUser');
        $data['form'] = Array('id' => Session::get('id'), 'profileid' => $id);
        $response = (array)$this->api->POSTAUTH($data);
        return redirect()->back()->withErrors(['message' => $response['message'], 'state' => $response['state']]);
    }

}
