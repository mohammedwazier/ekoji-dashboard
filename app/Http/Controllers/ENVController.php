<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class ENVController extends Controller
{

    public static function link($link){
        $linkVar = array(
            'local' => 'http://localhost:9000/api/v1/',
            'server' => 'http://180.131.147.103:9000/api/v1/',
            'production' => 'https://prxapi.nenggala.id/api/v1/'
        );
        $env = App::environment();
        return $linkVar[$env].$link;
    }

    public static function image($image){
        $linkImage = array(
            'local' => 'http://localhost:9000/files/',
            'server' => 'http://180.131.147.103:9000/files/',
            'production' => 'https://prxapi.nenggala.id/files/'
        );
        $env = App::environment();
        return $linkImage[$env].$image;
    }
}
