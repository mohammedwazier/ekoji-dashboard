@php
	$image = !empty($data['user']['photo']) ? $data['user']['photolink'] : asset('assets/img/faces/ayo-ogunseinde-2.jpg');
@endphp

	<div class="bg-header"></div>
	<div class="contentMain">
		<div class="content m-5">
			<div class="row">
				<div class="col-4 pl-4">
					<div class="profilePhotoDetail" style="background: url('{{$image}}') no-repeat; background-position: center center; background-size: cover;"></div>
				</div>
				<div class="col">
					<h3 style="color: #FFF;margin-bottom: 0!important;">{{ucfirst($data['user']['nama'])}}</h3>
					<span style="color: #FFF">Role : {{$data['user']['role']}}</span>
					<div class="card">
						<div class="card-body">
							<div class="form-group">
								<label>No HP</label>
								<input type="text" disabled class="form-control" value="+{{$data['user']['nohp']}}">
							</div>

							<div class="form-group">
								<label>Username</label>
								<input type="text" disabled class="form-control" value="{{$data['user']['username']}}">
							</div>

							<div class="form-group">
								<label>NIK</label>
								<input type="text" disabled class="form-control" value="{{$data['user']['nik']}}">
							</div>

							<div class="form-group">
								<label>Saldo Nexus</label>
								<input type="text" disabled class="form-control" value="{{$data['user']['saldo_nexus']}}">
							</div>

							<div class="form-group">
								<label>Status Akun</label>
								<input type="text" disabled class="form-control" value="{{MainController::UserStatus($data['user']['isactive'])}}">
							</div>

							<div class="form-group">
								<label>Created</label>
								<input type="date" disabled class="form-control" value="{{substr($data['user']['created'], 0, 10)}}">
							</div>

							<div class="form-group">
								@if(in_array(strtolower(Session::get('role')), ['superadmin', 'admin']) && $data['user']['id'] !== Session::get('id'))
									@if($data['user']['isactive'] === 1)
									<a href="{{route('SuspendUser', ['id' => $data['user']['id']])}}" class="btn btn-warning suspend">Suspend</a>
									@elseif($data['user']['isactive'] === 0)
									<a href="{{route('UnsuspendUser', ['id' => $data['user']['id']])}}" class="btn btn-success suspend">Unsuspend</a>
									@endif
									<a href="{{route('DeleteUser', ['id' => $data['user']['id']])}}" class="btn btn-danger delete">Delete</a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					History Transaksi
					<table class="table">
                    	<thead>
                          	<tr>
                            <th scope="col">No</th>
                            <th scope="col">ID Transaksi</th>
                            <th scope="col">Judul</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created Trx</th>
                          	</tr>
                    	</thead>
                        <tbody>
                            @foreach ($data['history'] as $key => $item)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>
                                    	<a href="{{route('TransaksiDetail', ['id' => $item['id_history']])}}">
                                    	{{$item['id_history']}}
                                    	</a>
                                    </td>
                                    <td>{{$item['judul']}}</td>
                                    <td>{{$item['state']}}</td>
                                    <td>{{Carbon\Carbon::create($item['created'])}}</td>
                                </tr>
                            @endforeach
                            @if(count($data['history']) === 0)
                            <tr>
                            	<td colspan="5" class="text-center"><i>No Transaction</i></td>
                            </tr>
                            @endif
                        </tbody>
                  	</table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.suspend, .delete').on('click', function(e){
				e.preventDefault();
				$('#profileDetail').modal('hide');
				swal({
				  text: 'Masukkan Password',
				  content: {
				  	element: 'input',
				  	attributes: {
				  		placeholder: 'Type your Password',
				  		type: 'password'
				  	}
				  }
				})
				.then(async password => {

					let url = `{{ENV::link('Util/validatePassword')}}/{{Session::get('id')}}/${password}/prexux`
					if(!password){
						$('#profileDetail').modal('show');
					}else{
						swal({
			                icon: "info",
			                text: 'Loading',
			                confirmButtonClass: "show-loading-icon",
			                cancelButtonClass: "show-loading-icon-delete",
			                buttons: false,
			                closeOnClickOutside: false
			            });
					  	fetch(url)
					  	.then(response => response.json())
					  	.then(response => {
				  			swal.close();
					  		if(response.state){
					  			window.location.href = $(this).attr('href');
					  		}else{
					  			swal({
				                    text: "Failed",
				                    icon: 'error'
				                })
					  		}
					  	})
					}
				})
			})
		})
	</script>
