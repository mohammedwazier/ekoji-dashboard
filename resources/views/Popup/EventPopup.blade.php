    <form method="POST" action="{{route('UpdateEvent')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="eventid" value="{{$data['event_id']}}">
        <input type="hidden" name="raw" value="{{json_encode($data)}}">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" id="formDetail">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Buat Event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Event</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Event" value="{{$data['event_nama']}}">
                    </div>

                    <div class="form-group">
                        <label>Cover Event</label>
                        <div class="custom-file">
                            <input type="file" name="cover" class="custom-file-input" accept="image/*" id="eventPopupID" img-target="eventCoverPopup">
                            <label class="custom-file-label eventCoverPopup" for="eventPopupID">Cover Event</label>
                        </div>
                    </div>
                    <div class="form-group m-3 text-center" style="position: relative;">
                        <img src="{{$data['event_coverlink']}}" id="eventCoverPopup" width="700px" />
                    </div>

                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="description" class="form-control editor">
                            {!! $data['event_keterangan'] !!}
                        </textarea>
                    </div>

                    @php
                    $listOpt1 = array(
                        array('id' => 1, 'keterangan' => 'Adding to User'),
                        array('id' => 2, 'keterangan' => 'Decrease from User'),
                        array('id' => 3, 'keterangan' => 'Decrease from Product')
                    );
                    @endphp

                    <div class="form-group">
                        <label>Metode Event</label>
                        <select name="method" class="form-control">
                            <option value="">Choose one</option>
                            <option value=""></option>
                            @foreach($listOpt1 as $key => $value)
                                <option {{$data['event_method'] === $value['id'] ? 'selected' : ''}} value="{{$value['id']}}">{{$value['keterangan']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label>Start Date</label>
                            <input type="date" value="{{\Carbon\Carbon::parse($data['event_startdate'])->format('Y-m-d')}}" name="startdate" class="form-control">

                            <label>End Date</label>
                            <input type="date" value="{{\Carbon\Carbon::parse($data['event_enddate'])->format('Y-m-d')}}" name="enddate" class="form-control">
                        </div>
                        <div class="col">
                            <label>Start Hour</label>
                            <input type="time" value="{{$data['event_starthour']}}" name="starthour" class="form-control">
                            <label>End Hour</label>
                            <input type="time" value="{{\Carbon\Carbon::parse($data['event_endhour'])->format('H:i')}}" name="endhour" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Type Value Event</label>
                        <select class="form-control edit-type" name="pay_method">
                            <option value="nexus">Nexus</option>
                            <option value="persen">Persen</option>
                        </select>
                    </div>

                    <div class="form-group type" id="nexusPopup">
                        <label>Nilai Nexus</label>
                        <input type="number" name="nilai" class="form-control nexusPopup" placeholder="Nilai Nexus" value="{{$data['event_value']}}" disabled>
                    </div>

                    <div class="form-group type" id="persenPopup">
                        <label>Nilai Persen</label>
                        <input type="number" min="0" max="100" name="nilai" class="form-control persenPopup" placeholder="Nilai Persen" value="{{$data['event_value']}}" disabled>
                    </div>

                    @php
                    $listOpt2 = array(
                        array('id' => 'register', 'keterangan' => 'When Register'),
                        array('id' => 'product', 'keterangan' => 'When Buy Product'),
                    );
                    @endphp

                    <div class="form-group">
                        <label>Type Event</label>
                        <select name="event_type" class="form-control">
                            <option value="">Choose one</option>
                            <option value=""></option>
                            @foreach($listOpt2 as $key => $value)
                                <option {{$data['event_type'] === $value['id'] ? 'selected' : ''}} value="{{$value['id']}}">{{$value['keterangan']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Max used Event</label>
                        <small class="form-text text-muted">Maksimal pemakaian Event, total maksimal user memakai event tersebut</small>
                        <input type="number" value="0" name="used" class="form-control" placeholder="Maksilam pemakaian Event" value="{{$data['event_max']}}">
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row w-100">
                        <div class="col">
                            <a href="{{route('DeleteEvent', ['id' => $data['event_id']])}}" class="btn btn-danger">Delete</a>
                        </div>
                        <div class="col d-flex flex-row-reverse">
                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function(){

            $('.type').hide();

            if($('.edit-type').val() === 'nexus'){
                $('#nexusPopup').show()
                $('.nexusPopup').attr('disabled', false);
            }

            $('.edit-type').on('change', function(){
                $('.type').hide();
                $('.type input').attr('disabled', true);
                if($(this).val() === 'nexus'){
                    setTimeout(function(){
                        $('#nexusPopup').show()
                        $('.nexusPopup').attr('disabled', false);
                    }, 200)
                }else{
                    setTimeout(function(){
                        $('#persenPopup').show()
                        $('.persenPopup').attr('disabled', false);
                    }, 200)
                }
            })

            $("#eventPopupID").change(function(){
                readURL(this, $(this).attr('img-target'))
            });

            let popupEditor = $('#formDetail .editor');
            const self = this;
            for (var i = 0; i < popupEditor.length; ++i) {
                ClassicEditor
                .create(popupEditor[i])
                // .then(editor => {
                // })
                .catch(error => {
                    console.log(error);
                })
            }

        })
    </script>