@php
// $cashflow = $res['cashflow'];
@endphp
<div class="row">
	<div class="col">
		<h3 class="mb-0">Detail Cashflow</h3>
	</div>
	<div class="w-100"></div>
	<div class="col">
		<table class="table table-responsive table-striped table-custom" style="overflow-y: auto">
			<thead>
				<tr>
					<th scope="col">No</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col">Tipe</th>
                    <th scope="col">Nominal</th>
                    <th scope="col">Debet</th>
                    <th scope="col">Kredit</th>
                    <th scope="col">Profile Nama</th>
                    <th scope="col">Akun Internal</th>
                    <th scope="col">Created</th>
                    <th scope="col">type</th>
				</tr>
			</thead>
			<tbody>
				@foreach($cashflow as $key => $value)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$value['cf_keterangan']}}</td>
						<td>{{$value['cf_tipe']}}</td>
						<td>{{$value['cf_nominal']}}</td>
						<td>{{$value['cf_debet']}}</td>
						<td>{{$value['cf_kredit']}}</td>
						<td>{{$value['prl_nama']}}</td>
						<td>{{$value['cf_internal_acc']}}</td>
						<td>{{$value['cf_created_at']}}</td>
						<td>{{$value['cf_mode']}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
{{-- {{dd($res)}} --}}