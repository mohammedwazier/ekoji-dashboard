@php
$data = $ret['data'];
@endphp
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLongTitle">Detail Mutasi External</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>
<form method="POST" action="{{route('UpdateDepositDetail')}}">
	@csrf
	<input type="hidden" name="service_id" value="{{$data['service_id']}}">
	<div class="modal-body">
		<div class="form-group">
			<label>Bank Service</label>
	    	<input type="text" class="form-control" value="{{$data['service_name']}}" disabled>
		</div>

		<div class="form-group">
			<label>Nama Rekening Akun</label>
	    	<input type="text" class="form-control" value="{{$data['account_name']}}" disabled>
		</div>

		<div class="form-group">
			<label>Nomor Rekening</label>
	    	<input type="text" class="form-control" value="{{$data['account_number']}}" disabled>
		</div>

		<div class="form-group">
			<label>Keterangan</label>
	    	<input type="text" class="form-control" value="{{$data['description']}}" disabled>
		</div>

		<div class="form-group">
			<label>Amount Data</label>
	    	<input type="text" class="form-control" value="{{MainController::rupiah($data['amount'])}}" disabled>
		</div>

		<div class="form-group">
			<label>Cek Mutasi Service ID</label>
	    	<input type="text" class="form-control" value="{{$data['service_id']}}" disabled>
		</div>

		<div class="form-group">
		    <label for="OptionData">Pilih Aksi</label>
		    <select class="form-control" name="status_deposit" id="OptionData" required>
		    	<option></option>
		      	<option value="1">Terima Transaksi</option>
		      	<option value="2">Masukkan ke Penampungan Salah Transfer | Transaksi Dibatalkan</option>
		    </select>
		</div>
		<div class="AccTrx">
			<div class="form-group">
			    <label>Pilih Profil Tujuan</label>
			    <select class="form-control" name="data_trx" id="SelectUser">
			    	<option></option>
			    	@foreach($ret['deposit'] as $key => $value)
			    		<option value="{{$value['trx_refid']}}" data-token="{{$value['prl_nama']}}">{{$value['prl_nama']}} | Topup : {{MainController::rupiah($value['dep_nominal'])}} | Expectation : {{MainController::rupiah($value['dep_total'])}}</option>
			    	@endforeach
			    </select>
			</div>

		    <div class="form-group" >
		    	<table class="table table-responsive table-striped table-custom">
		    		<thead>
		    			<tr>
	                        <th scope="col">Nama Profile</th>
	                        <th scope="col">Nomor HP</th>
	                        <th scope="col">Username</th>
	                        <th scope="col">Topup Nominal</th>
	                        <th scope="col">Kode Unik</th>
	                        <th scope="col">Topup Expectation</th>
	                        <th scope="col">Transaksi Invoice</th>
	                        <th scope="col">Create Transaksi</th>
	                	</tr>
		    		</thead>
		    		<tbody>
		    			@foreach($ret['deposit'] as $key => $value)
		    			<tr id="_detail_data_{{$value['trx_refid']}}" class="detailDataDeposit">
			    			<td>
			    				<a href="{{route('DetailUser', ['id' => $value['prl_profile_id']])}}" target="_blank">
			    					{{$value['prl_nama']}}
	                            </a>
			    			</td>
			    			<td>+{{$value['prl_nohp']}}</td>
			    			<td>{{$value['prl_username']}}</td>
			    			<td>{{MainController::rupiah($value['dep_nominal'])}}</td>
			    			<td>{{$value['dep_kode_unik']}}</td>
			    			<td>{{MainController::rupiah($value['dep_total'])}}</td>
			    			<td>
			    				<a href="{{route('TransaksiDetail', ['id' => $value['trx_refid']])}}" target="__construct">
			    				{{$value['trx_invoice']}}
			    				</a>
			    			</td>
			    			<td>{{\Carbon\Carbon::parse($value['trx_created_at'])}}</td>
		    			</tr>
		    			@endforeach
		    		</tbody>
		    	</table>
		    </div>
		</div>

	</div>
	<div class="modal-footer">
	    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	    <input type="submit" class="btn btn-primary" id="SubmitButton" disabled value="Save change">
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$('.detailDataDeposit, .AccTrx').hide();
		$('#SelectUser').selectpicker({
			style: 'btn-light',
			liveSearch: true
		});
		// $('select').selectpicker();
		$('#OptionData').on('change', function(){
			let el = $(this);
			let val = Number(el.val());
			if(val === 1){
				$('.AccTrx').show();
			}else if(val === 2){
				$('#SubmitButton').attr('disabled', false)
				$('.AccTrx').hide();
			}else{
				$('#SubmitButton').attr('disabled', true)
				$('.AccTrx').hide();
			}
		})
		$('#SelectUser').on('change', function(){
			let targ = $(this);
			$('.detailDataDeposit').hide();
			if(!targ.val()){
				$('#SubmitButton').attr('disabled', true)
			}else{
				$('#_detail_data_'+targ.val()).show();
				$('#SubmitButton').attr('disabled', false)
			}
		})
	})
</script>
{{-- {{dd($ret)}} --}}