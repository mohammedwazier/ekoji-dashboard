<div id="placementEditData">
    <form action="{{route('SavePlacement')}}" method="POST" id="updatePlacement">
        @csrf
        <input type="hidden" name="id" value="{{$id}}">
        <div class="form-group m-3 text-center" style="position: relative;">
            <div id="parentCanvas" class="text-center">
                <div class="dragContent clickedContent nama blink_border" id="MainNameEditPlace" style="font-size:13px;" data-type="nameEdit">
                    The Brown Fox Jumps Over The Lazy Dog
                </div>
                <div class="dragContent clickedContent blink_border" id="NomorSuratEditPlace" style="font-size: 12px;" data-type="nomorEdit">
                    NOMOR : PREI/SEM/KODE_PRODUK/{{MainController::romanize(\Carbon\Carbon::now()->month)}}/{{\Carbon\Carbon::now()->year}}/0001
                </div>
                <div class="form-group text-center">
                    <img src="{{$image}}" id="produkImgPlacement" width="700px" />
                </div>
            </div>
            <input type="hidden" id="data-imageEdit" name="data_image" class="data-image" />
            <input type="hidden" id="data-nameEdit" name="data_name" />
            <input type="hidden" id="data-nomorEdit" name="data_nomor" />
        </div>

        <div class="w-100"></div>
        <div class="form-group m-3">
            <label>Option Nama</label>
            <select class="form-control fontSize" form-target="MainNameEditPlace">
                @for($idx = 13; $idx < 38; $idx++){
                    <option {{$idx == 13 ? 'selected' : ''}} value="{{$idx}}">{{$idx}}</option>
                @endfor
            </select>
            <label>Warna</label>
            {{-- {{$data}} --}}
            <select class="form-control colorChange" form-target="MainNameEditPlace">
                <option selected value="b">Black</option>
                <option value="w">White</option>
            </select>
            <button type="button" class="center-y btn btn-success" form-target="MainNameEditPlace">Center Y</button>
            <button type="button" class="center-x btn btn-success" form-target="MainNameEditPlace">Center X</button>
        </div>
        <div class="form-group m-3">
            <label>Option Nomor</label>
            <select class="form-control fontSize" form-target="NomorSuratEditPlace">
                <option value="8">8</option>
                <option value="10">10</option>
                <option selected value="12">12</option>
                <option value="14">14</option>
                <option value="16">16</option>
                <option value="32">32</option>
            </select>
            <label>Warna</label>
            <select class="form-control colorChange" form-target="NomorSuratEditPlace">
                <option selected value="b">Black</option>
                <option value="w">White</option>
            </select>
            <button type="button" class="center-y btn btn-success" form-target="NomorSuratEditPlace">Center Y</button>
            <button type="button" class="center-x btn btn-success" form-target="NomorSuratEditPlace">Center X</button>
        </div>
        <div class="form-group row m-1">
            <div class="col">
                <input type="submit" class="btn btn-primary" value="Simpan">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            <div class="col text-right">
                <button type="button" class="btn btn-secondary btn-back">Kembali</button>
            </div>
        </div>
    </form>
</div>
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/js/PlacementCertificate.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        const PlacementData = JSON.parse(@json($data));
        var img = new Image();
        img.onload = function(){
            const { width, height } = img;
            var w = width;
            var h = height;
            var r = gcd (w, h);
            let rw = w/r;
            let rh = h/r;
            let target = 'produkImgPlacement';
            let widthContent = width;
            let heightContent = height
            const { image } = PlacementData;

            let data = {
                ratioWidth: image.originalRatio.w,
                ratioHeight: image.originalRatio.h,
                width: width,
                height: height,
                target: target
            }
            updateLoad(data);
            setupPlacement('MainNameEditPlace', PlacementData.nama);
            setupPlacement('NomorSuratEditPlace', PlacementData.nomor);
        }
        img.src = $('#produkImgPlacement').attr('src');

        $('.btn-back').click(function(){
            $('#placementDetailForm').fadeOut();
            $('#detailData').fadeIn();

        })
        $('#updatePlacement').on('submit', function(){
            $('#produkDetail').modal('toggle');
            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });
        })
    })
</script>