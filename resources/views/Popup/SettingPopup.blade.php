<form method="POST" action="{{route('UpdateSetting')}}" enctype="multipart/form-data">
    @csrf
    <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Buat Setting Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <input type="hidden" name="id" value="{{$data['st_id']}}">

                <div class="form-group mb-3">
                    <label>Kode</label>
                    <input type="text" name="kode" class="form-control" placeholder="Kode. Cont: st_otp" required value="{{$data['st_kode']}}">
                </div>

                <div class="form-group mb-3">
                    <label>Keterangan</label>
                    <textarea name="keterangan" class="form-control" placeholder="Description" required>{{$data['st_keterangan']}}</textarea>
                </div>

                <div class="form-group mb-3">
                    <label>Kode</label>
                    @php
                    $list = array(
                        'List', 'Integer', 'Float', 'Boolean', 'String', 'Object', 'Date', 'Time', 'Html'
                    );
                    @endphp
                    <select name="typevalue" class="form-control" required>
                        <option value="">--PILIH--</option>
                        @foreach($list as $key => $value)
                            <option {{strtolower($value) === $data['st_typevalue'] ? 'selected' : ''}} value="{{strtolower($value)}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group mb-3">
                    <label>Value</label>
                    <textarea name="value" class="form-control" placeholder="Value Data" required>{{$data['st_value']}}</textarea>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input
                        type="checkbox"
                        class="custom-control-input changed"
                        id="customSwitches"
                        name="active"
                        checked="{{$data['st_isactive'] === 1 ? true : false}}"
                        >
                        <label class="custom-control-label" for="customSwitches">Active</label>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="btnSucc">Simpan</button>
            </div>
        </div>
    </div>
</form>