@php
$data = $ret['data'];
$category = $ret['category'];
@endphp
{{-- {{dd($ret['placement'])}} --}}
<div  id="ProdukPopupDataDetail">
	<div class="content m-5">
		<div class="row">
			<div class="col-md-4 text-center">
				<img src="{{$data['cover_link']}}">
				<br />
				<hr />
				<h4>Cover Produk</h4>
			</div>
			<div class="col">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>
								Kode Produk
							</label>
							<input class="form-control" type="text" disabled="disabled" value="{{$data['kodeproduk']}}">
						</div>

						<div class="form-group">
							<label>
								Nama Produk
							</label>
							<input class="form-control editable" type="text" disabled="disabled" value="{{$data['namaproduk']}}">
						</div>

						<div class="form-group">
							<label>
								Keterangan
							</label>
							<textarea class="form-control editorUpdate" id="editorUpdate" disabled="disabled">
								{!! $data['keteranganproduk'] !!}
							</textarea>
						</div>

						<label>
							Status Produk
						</label>
						<div class="custom-control custom-switch">
							<input
								type="checkbox"
								class="custom-control-input editable"
								id="customSwitch1"
								value="{{$data['isactiveproduk']}}"
								checked="{{$data['isactiveproduk']}}"
								disabled="true"
							>
							<label class="custom-control-label" for="customSwitch1">
								{{MainController::statusEnabled($data['isactiveproduk'])}}
							</label>
						</div>

						<div class="form-group">
							<label>Group Produk</label>
                            <select class="custom-select mr-sm-2 editable" id="changeProduk" name="tipeproduk" disabled="true">
                                @foreach($category as $key => $item)
                                	<option
                                	value="{{$item['groupid']}}"
                                	id="val{{$key}}"
                                	{{$data['groupid'] === $item['groupid'] ? 'selected' : ''}}
                                	>
                                		{{$item['nama']}}
                                	</option>
                                @endforeach
                            </select>
						</div>
						{{-- {{dd($ret)}} --}}
						@if(strpos(strtolower($data['tipegroup']), 'certificate'))
							{{-- {{dd($data)}} --}}
							<div class="form-group">
	                            <label>Gambar Produk Sertifikat</label>
	                        </div>
	                        <div class="form-group m-3 text-center">
	                        	{{-- {{ENV::image($data['produk'])}} --}}
	                            <img src="{{$data['produk_link']}}" id="produkImg" width="300px" />
	                        </div>
						@elseif(strpos(strtolower($data['tipegroup']), 'presentasi'))
						@elseif(strpos(strtolower($data['tipegroup']), 'book'))
						@elseif(strpos(strtolower($data['tipegroup']), 'profisiensi'))
						@endif

						<div class="form-group row">
							<div class="col">
								<label>
									Produk Viewed
								</label>
								<input class="form-control" type="text" disabled="disabled" value="{{$data['produkview']}}">
							</div>
							<div class="col">
								<label>
									Produk Buy
								</label>
								<input class="form-control" type="text" disabled="disabled" value="{{(int)$data['produkbuy']}}">
							</div>
						</div>

						<div class="form-group">
							<label>
								Link Materi
							</label>
							<input class="form-control editable" type="text" disabled="disabled" value="{{$data['linkproduk']}}">
						</div>

						<div class="form-group">
							<label>
								Harga <sub>(Nexus)</sub>
							</label>
							<input class="form-control editable" type="text" disabled="disabled" value="{{$data['produkharga']}}">
						</div>

						<div class="form-group">
							<label>
								Pemateri
							</label>
							<input class="form-control" type="text" disabled="disabled" value="{{$data['namapemateri']}}">
						</div>
						<div class="form-group custom-switch">
							<input
								type="checkbox"
								class="custom-control-input"
								id="EditableProduk"
								value="0"
							>
							<label class="custom-control-label" for="EditableProduk">
								Edit Produk
							</label>
						</div>
						<div class="form-group row">
							<div class="col">
								<input type="submit" class="btn btn-success editable" value="Simpan" disabled="true">
								<input type="button" class="btn btn-primary edit_placement" value-id="{{$data['produkid']}}" value="Edit Placement">
							</div>
							<div class="col text-right">
								{{-- <input type="submit" class="btn btn-danger" value="Delete"> --}}
								<a href="{{route('DeleteProduk', ['id' => $data['produkid']])}}" class="btn btn-danger delete">Delete</a>
							</div>
						</div>
						<div class="form-group">
						</div>


					</div>
				</div>
				{{-- @if(strpos(strtolower($data['tipegroup']), 'profisiensi'))
				@elseif(strpos(strtolower($data['tipegroup']), 'certificate'))
				@elseif(strpos(strtolower($data['tipegroup']), 'book'))
				@elseif(strpos(strtolower($data['tipegroup']), 'presentasi'))
				@else
				@endif --}}
			</div>
			{{-- <div class="w-100"></div>
			<div class="col">
				@if(strpos(strtolower($data['tipegroup']), 'profisiensi'))
				@elseif(strpos(strtolower($data['tipegroup']), 'certificate'))
				@elseif(strpos(strtolower($data['tipegroup']), 'book'))
				@elseif(strpos(strtolower($data['tipegroup']), 'presentasi'))
				@else
				@endif
			</div> --}}
		</div>
	</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').on('click', function(e){
			e.preventDefault();
			$('#produkDetail').modal('hide');
			swal({
			  text: 'Masukkan Password',
			  content: {
			  	element: 'input',
			  	attributes: {
			  		placeholder: 'Type your Password',
			  		type: 'password'
			  	}
			  }
			})
			.then(async password => {

				let url = `{{ENV::link('Util/validatePassword')}}/{{Session::get('id')}}/${password}/prexux`
				if(!password){
					$('#produkDetail').modal('show');
				}else{
					swal({
		                icon: "info",
		                text: 'Loading',
		                confirmButtonClass: "show-loading-icon",
		                cancelButtonClass: "show-loading-icon-delete",
		                buttons: false,
		                closeOnClickOutside: false
		            });
				  	fetch(url)
				  	.then(response => response.json())
				  	.then(response => {
			  			swal.close();
				  		if(response.state){
				  			window.location.href = $(this).attr('href');
				  		}else{
				  			swal({
			                    text: "Failed",
			                    icon: 'error'
			                })
				  		}
				  	})
				}
			})
		})
	})
	$(document).ready(async function(){
		{{-- console.log(@json($ret['placement'])) --}}
		var data;
		await ClassicEditor
		.create(document.querySelector('.editorUpdate'))
		.then(editor => {
			data = editor;
			editor.isReadOnly = true;
		})
		.catch(error => {
			console.log(error);
		})

		$('#EditableProduk').on('change', function(){
			let value = $(this).prop('checked');

			$('.editable').attr('disabled', !value);
		})

		$('.edit_placement').on('click', function(){
			var body = {
                _token: "{!! csrf_token() !!}",
                data: @json($ret['placement']['placement_data']),
                image: "{{$data['produk_link']}}",
                id: "{{$data['produkid']}}"
            }
            $.ajax({
                url: `{{route('DetailPlacement')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                	$('#placementDetailForm').html(data);
                	$('#detailData').fadeOut();
                	setTimeout(function(){
                		$('#placementDetailForm').fadeIn()
                	},200)
                },
                error: function(err){
                    alert('Failed to Get Placement Detail');
                }
            })
		});

	})
</script>
</div>