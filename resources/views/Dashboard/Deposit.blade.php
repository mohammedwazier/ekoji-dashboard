@extends('main', [
    'class' => '',
    'elementActive' => 'deposit'
])

@section('content')
@php
$data = $ret['data'];

// dd($ret);
$deposit = $ret['statistic']['deposit'];
$mutasi = $ret['statistic']['mutasi_external'];
@endphp
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / Deposit List
            </div>
            <div class="w-100 mt-3"></div>
        </div>
        <div class="row">
            @foreach($deposit as $key => $value)
            {{-- {{dd($value)}} --}}
            <div class="col">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-globe text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">{{MainController::statusDeposit($value['dep_status'])}}</p>
                                    <p class="card-title">
                                        {{$value['count']}}
                                    <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            Total Nilai : <b>{{MainController::rupiah($value['sum'])}}</b>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="w-100 mt-3"></div>

            @foreach($mutasi as $key => $value)
            {{-- {{dd($value)}} --}}
            <div class="col-4">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-globe text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">{{MainController::statusMutasi($value['mutasi_status'])}}</p>
                                    <p class="card-title">
                                        {{$value['count']}}
                                    <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            {{-- <i class="fa fa-refresh"></i> --}} Total Nilai : <b>{{MainController::rupiah($value['sum'])}}</b>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Search
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('SearchDeposit')}}">
                            @csrf
                            <div class="form-group">
                                <input type="search" class="form-control" name="search" placeholder="Search Anything" value="{{$errors->has('search') ? $errors->first('value') : ''}}" required />
                                @if($errors->has('search'))
                                    <span class="badge bg-green">{{$errors->first('message')}} <a href=""><i class="nc-icon nc-simple-remove"></i></a></span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col-8">
                <div class="card">
                    <div class="card-header">
                        <h5>Deposit Queue</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive table-striped table-custom" style="overflow-y: hidden;overflow-x: auto;">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Deposit ID</th>
                                <th scope="col">Akun User</th>
                                <th scope="col">Total Bayar</th>
                                <th scope="col">Rekening Tujuan</th>
                                <th scope="col">Kode Unik</th>
                                <th scope="col">Status</th>
                                <th scope="col">Expired</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @php
                                $iter = 1;
                                @endphp
                                @foreach ($data as $key => $item)
                                @if($item['status'] !== 3)
                                    {{-- @if($item['status'] != 4) --}}
                                        <tr>
                                            <th scope="row">{{$iter}}</th>
                                            <td>
                                                <a href="{{route('DetailDeposit', ['id' => $item['iddeposit']])}}">
                                                    {{$item['iddeposit']}}
                                                </a>
                                            </td>
                                            <td>{{$item['namaakun']}}</td>
                                            <td>{{MainController::rupiah($item['totalbayar'])}}</td>
                                            <td>{{$item['namarekening']}}</td>
                                            <td>{{$item['kodeunik']}}</td>
                                            <td>
                                                {{MainController::statusDeposit($item['status'])}}
                                            </td>
                                            <td>{{Carbon\Carbon::create($item['expired'])}}</td>
                                            <td class="text-center">
                                                    <a>
                                                        @if(MainController::checkStatusDeposit($item['status']))
                                                        {{-- {{dd($item)}} --}}
                                                        <form method="post" action="{{route('UpdateDeposit')}}" enctype="multipart/form-data">
                                                            @csrf
                                                            {{-- <input type="hidden" name="items" id="{{$item['iddeposit']}}" value="{{json_encode($item)}}">
                                                             --}}
                                                            <input type="hidden" name="id" value="{{$item['iddeposit']}}" />
                                                            <div class="form-group">
                                                                <div class="custom-control custom-switch">
                                                                    <input
                                                                    type="checkbox"
                                                                    class="custom-control-input changed"
                                                                    {{-- valueid="{{$item['iddeposit']}}" --}}
                                                                    name="status"
                                                                    {{-- value="{{$item['iddeposit']}}" --}}
                                                                    id="customSwitches"
                                                                    >
                                                                    <label class="custom-control-label" for="customSwitches">Proses</label>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        @endif
                                                    </a>
                                                </td>
                                            </tr>
                                            @php
                                            $iter++;
                                            @endphp
                                    @endif

                                    @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>Deposit Expired</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive table-striped table-custom" style="overflow-y: hidden;overflow-x: auto; width:100%">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Deposit ID</th>
                                <th scope="col">Akun User</th>
                                <th scope="col">Total Bayar</th>
                                <th scope="col">Rekening Tujuan</th>
                                <th scope="col">Kode Unik</th>
                                <th scope="col">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                @php
                                $iter = 1;
                                @endphp
                                @foreach ($data as $key => $item)
                                @if($item['status'] === 3)
                                        <tr>
                                            <th scope="row">{{$iter}}</th>
                                            <td>
                                                <a href="{{route('DetailDeposit', ['id' => $item['iddeposit']])}}">
                                                    {{$item['iddeposit']}}
                                                </a>
                                            </td>
                                            <td>{{$item['namaakun']}}</td>
                                            <td>{{MainController::rupiah($item['totalbayar'])}}</td>
                                            <td>{{$item['namarekening']}}</td>
                                            <td>{{$item['kodeunik']}}</td>
                                            <td>
                                                {{MainController::statusDeposit($item['status'])}}
                                            </td>
                                            </tr>
                                            @php
                                            $iter++;
                                            @endphp
                                    @endif
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        {{-- <div class="row align-items-center">
                            <div class="col"> --}}
                                <h5>Mutasi tidak valid</h5>
                            {{-- </div>
                            <div class="col text-right">
                            </div> --}}
                        {{-- </div> --}}
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive table-striped table-custom" style="overflow-y: hidden;overflow-x: auto;">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Bank</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Mutasi Status</th>
                                <th scope="col">Detail</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($ret['mutasi'] as $key => $value)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>{{$value['service_name']}}</td>
                                    <td>{{MainController::rupiah($value['amount'])}}</td>
                                    <td>{{$value['mutasi_status']}}</td>
                                    <td class="text-center">
                                        <button class="btn btn-success data_mutasi" data-target="{{$value['service_id']}}">
                                        Detail
                                        </button>
                                        <input type="hidden" id="{{$value['service_id']}}"  value="{{json_encode($value)}}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="w-100 mb-3"></div>
        </div>
    </div>

    <div class="modal fade" id="mutasi_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content targetModalData">
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script type="text/javascript">
    function popupData(e){
        let el = e.target;
        let target = $(el).attr('data-target');
        let jsonData = JSON.parse($(`#${target}`).val());
        jsonData = {
            _token: "{!! csrf_token() !!}",
            profileid: '{{Session::get('id')}}',
            ...jsonData
        }
        let targetModal = $('#mutasi_modal');
        let body = jsonData;
        $.ajax({
            url: `{{route('popupDetailMutasi')}}`,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': `{!! csrf_token() !!}`
            },
            data: body,
            success: function(data, textStatus, jQxhr){
                targetModal.find('.targetModalData').html(data);
                targetModal.modal();
            },
            error: function(err){
                console.log(err);
            }
        })

    }
    $(document).ready(function(){
        $('.data_mutasi').click(popupData);
        $('.changed').on('change', function(e){
            e.preventDefault();
            /* var ini = this;
            var valueid = $(this).attr('valueid');
            var val = $(this).val();
            var status = $(this).is(':checked');
            var numberStatus = Number(status);
            numberStatus = numberStatus = */
            // == 1 ? 2 : numberStatus;
            // console.log(numberStatus)
            /* var body = {
                _token: "{!! csrf_token() !!}",
                id: valueid,
                status: numberStatus
            } */
            const _this = this;
            swal({
                title: "Are you sure?",
                text: "Accept this Transaction",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(willDelete => {
                if(willDelete){
                    swal({
                        icon: "info",
                        text: 'Loading',
                        confirmButtonClass: "show-loading-icon",
                        cancelButtonClass: "show-loading-icon-delete",
                        buttons: false,
                        closeOnClickOutside: false
                    });
                    $($(_this).closest('form')).submit();
                    // $(this)
                    /* $.ajax({
                        url: `{{route('UpdateDeposit')}}`,
                        type: 'POST',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                        },
                        data: body,
                        success: function(data, textStatus, jQxhr){
                            console.log(data)
                            // location.reload();
                        },
                        error: function(err){
                            console.log(err)
                            // location.reload();
                        }
                    }) */

                }else{
                    $(ini).prop('checked', false);
                }
            })
        })
    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('state'))
            @if($errors->first('state'))
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @else
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @endif
        @endif
    })
</script>
@endpush