@extends('main', [
    'class' => '',
    'elementActive' => 'userguide'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / Userguide
            </div>
        </div>
        <div class="row">
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>Userguide List</h5>
                            </div>
                            <div class="col text-right">
                                <button
                                class="btn btn-success mr-auto"
                                data-toggle="modal"
                                data-target="#CreateUserGuide"
                                data-backdrop="static"
                                >
                                    ADD +
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-custom" id="tableData">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Link</th>
                                <th scope="col" class="text-right">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $value)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$value['guidename']}}</td>
                                    <td><a href="{{ENV::image($value['file'])}}" target="_blank">Link</a></td>
                                    <td><a href="{{route('UserGuideDelete', ['guideid' => $value['guideid']])}}" onclick="return confirm('Sure?')">Hapus</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="CreateUserGuide" tabindex="-1" role="dialog" aria-labelledby="CreateSettingTitle" aria-hidden="true">
        <form method="POST" action="{{route('UserGuideCreate')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Buat Setting Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                         <div class="form-group">
                            <label for="exampleInputEmail1">Nama Userquide</label>
                            <input type="text" name="ug_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Upload Userguide</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" id="inputGroupFile01" required>
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="btnSucc">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="popupDetail" tabindex="-1" role="dialog" aria-labelledby="createProfileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5">
            <div class="modal-content">
                <div id="detailData"></div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tableData').DataTable();

        $('#inputGroupFile01').on('change', function(e) {
            var fileName = e.target.files[0].name;
            $(this).closest('div').find('label').html(fileName);
        })

        $('.detail-data').on('click', function(e){
            e.preventDefault();
            var body = {
                _token: "{!! csrf_token() !!}",
                kode: $(this).attr('id')
            }
            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });

            
            $.ajax({
                url: `{{route('PopupSetting')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                    $('#detailData').html(data)
                    $('#popupDetail').modal({
                        show: true
                    });
                    swal.close()
                },
                error: function(err){
                    console.log(err)
                }
            })
        })
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('state'))
            @if($errors->first('state'))
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @else
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @endif
        @endif
    })
</script>
@endpush