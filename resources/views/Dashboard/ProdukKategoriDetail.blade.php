@extends('main', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
{{-- {{dd($data['urlImage'])}} --}}
<div class="content">
    <form method="POST"  enctype="multipart/form-data" action="{{route('UpdateKategori')}}">
        <input type="hidden" name="id" value="{{$data['groupid']}}" />
        @csrf
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / <a href="{{route('Produk')}}">Produk</a> / Detail Kategori / {{$data['nama']}}
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>Kategori Detail</h5>
                            </div>
                            <div class="col text-right">
                                <button type="submit" class="btn btn-success mr-auto">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" name="name" class="form-control" value="{{$data['nama']}}" placeholder="Nama Kategori"  required>
                        </div>
                        <label>Gambar Kategori</label>
                        <div class="custom-file">
                            <input type="file" name="image"  class="custom-file-input" id="kategoriPic" accept="image/*">
                            <label class="custom-file-label kategoriImg" for="kategoriPic">{{$data['gambar']}}</label>
                        </div>
                        <div class="form-group m-3 text-center">
                            <img id="kategoriImg" width="300px" for="kategoriPic" src="{{$data['urlImage']}}" loading="lazy" />
                        </div>
                        <div class="form-group m-3 text-right">
                            {{-- <a href="" --}}
                            <button class="btn btn-secondary mr-auto">
                                Kembali
                            </button>
                            <a class="delete" href="{{route('DeleteKategori', ['id' => $data['groupid']])}}">
                                <button class="btn btn-danger mr-auto">
                                    Hapus
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@push('scripts')
<script>

    const hasSupport = 'loading' in HTMLImageElement.prototype;
    document.documentElement.className = hasSupport ? 'pass' : 'fail';
    document.querySelector('span').textContent = hasSupport;

    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var file = document.querySelector('input[type=file]').files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image;
                img.onload = function() {
                    const { width, height } = img;
                    if(width / height !== 1){
                        swal("Gagal", "Gambar yang dipilih rasionya bukan 1:1", "error");
                        $(input).val(null);
                        $(`.${target}`).html('Pilih Gambar Kategori');
                        $(`#${target}`).attr('src', "")

                    }else{
                        $(`.${target}`).html(file.name);
                        $(`#${target}`).attr('src', e.target.result);
                    }
                };

                img.src = reader.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#kategoriPic").change(function(){
        readURL(this, 'kategoriImg');
    });
    $(document).ready(() => {
        $('.delete').click(function(e){
            e.preventDefault();
            var link = $(this).attr('href');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal({
                        icon: "info",
                        text: 'Loading',
                        confirmButtonClass: "show-loading-icon",
                        cancelButtonClass: "show-loading-icon-delete",
                        buttons: false,
                        closeOnClickOutside: false

                    });
                    location.href = link;
                } else {
                    swal("Your imaginary file is safe!");
                }
            });
        })
    })

</script>
@endpush