@extends('main', [
    'class' => '',
    'elementActive' => 'profile'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / Profile
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card card-stats">
                    <div class="card-header">
                        Search
                    </div>
                    <div class="card-body">
                        <form method="POST" id="searchForm" action="{{route('SearchProfile')}}">
                            @csrf
                            <div class="form-group">
                                <input type="search" class="form-control" name="search" placeholder="Search Anything" value="{{$errors->has('search') ? $errors->first('value') : ''}}" required />
                                @if($errors->has('search'))
                                    <span class="badge bg-green">{{$errors->first('message')}} <a href=""><i class="nc-icon nc-simple-remove"></i></a></span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>

            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>List User</h5>
                            </div>
                            <div class="col text-right">
                                <button
                                class="btn btn-success mr-auto"
                                data-toggle="modal"
                                data-target="#createProfile"
                                data-backdrop="static"
                                >
                                    ADD +
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-custom">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">No HP</th>
                                <th scope="col">Username</th>
                                <th scope="col">Role</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @php
                                $iterator = $response['start'];
                                @endphp
                                @foreach ($data as $key => $item)
                                    <tr>
                                        <th scope="row">{{$iterator+1}}</th>
                                        <td>
                                            <a href="" class="profile-detail" id="{{$item['id']}}">
                                                {{$item['nama']}}
                                            </a>
                                        </td>
                                        <td>{{$item['nohp']}}</td>
                                        <td>{{$item['username']}}</td>
                                        <td>{{$item['role']}}</td>
                                        <td class="text-center">
                                            <a href="" class="profile-detail" id="{{$item['id']}}">
                                            Detail
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                    $iterator++;
                                    @endphp
                                @endforeach
                            </tbody>
                            {{-- {{dd($errors)}} --}}
                            @if(!$errors->has('search'))
                            <tfoot>
                                <tr>
                                    <td colspan="10">
                                        <div>
                                            {{$data->links()}}
                                            <small>
                                                <i>Page : {{$response['current_page']}} | Total Data : {{$response['total']}}</i>
                                            </small>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                            @endif
                          </table>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>
        </div>
    </div>

    <div class="modal fade" id="createProfile" tabindex="-1" role="dialog" aria-labelledby="createProfileTitle" aria-hidden="true">
        <form method="POST" action="{{route('CreateProfile')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Buat Akun Profile</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group mb-3">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                        </div>

                        <label>No HP</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">+</span>
                            </div>
                            <input type="number" name="nohp" class="form-control" maxlength="14" placeholder="Number Phone : 6281122334455" required>
                        </div>

                        <label>Email</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">@</span>
                            </div>
                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                        </div>

                        <div class="form-group mb-3">
                            <label>Tanggal Lahir</label>
                            <input type="date" name="tgllahir" class="form-control" required>
                        </div>

                        <div class="form-group mb-3">
                            <label>Tempat Lahir</label>
                            <input type="text" name="tempatlahir" class="form-control" placeholder="Tempat Lahir" required>
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" name="alamat" placeholder="Alamat Lengkap" rows="3"></textarea>
                        </div>

                        <div class="form-group mb-3">
                            <label>Gender</label>
                            <select class="form-control" name="gender" required>
                                <option></option>
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                                <option value="Not Sure">Not Sure</option>
                                <option value="I Dunno">I Dunno</option>
                            </select>
                        </div>

                        <hr />

                        <div class="form-group mb-3">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" placeholder="Username" required>
                        </div>

                        <div class="form-group mb-3">
                            <label>Password</label>
                            <input type="password" name="password" minlength="8" class="form-control" placeholder="Password" required>
                        </div>

                        <div class="form-group mb-3">
                            <label>Role</label>
                            <select class="form-control" name="role" required>
                                <option></option>
                                <option value="superadmin">Superadmin</option>
                                <option value="admin">Admin</option>
                                <option value="user">User</option>
                                <option value="pemateri">Pemateri</option>
                            </select>
                        </div>

                        <label>Gambar Profil</label>
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="profilPic" accept="image/*" required>
                            <label class="custom-file-label profilImg" for="profilPic">Pilih Gambar Profil</label>
                        </div>
                        <div class="form-group m-3 text-center">
                            <img src="" id="profilImg" width="300px" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="btnSucc">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="profileDetail" tabindex="-1" role="dialog" aria-labelledby="createProfileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5">
            <div class="modal-content">
                <div id="detailData"></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script>
    $("#profilPic").change(function(){
        readURL(this, 'profilImg', 0, 0);
    });
    $(document).ready(function(){
        @if(!empty($findDetail))
        var bodyDetail = {
            _token: "{!! csrf_token() !!}",
            profileid: `{{$id}}`
        }
        $.ajax({
            url: `{{route('getDetailUser')}}`,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': `{!! csrf_token() !!}`
            },
            data: bodyDetail,
            success: function(data){
                $('#detailData').html(data)
                $('#profileDetail').modal({
                    show: true
                }); 
            },
            error: function(err){
                console.log(err)
            }
        })

        @endif
        $('.profile-detail').on('click', function(e){
            e.preventDefault();
            var body = {
                _token: "{!! csrf_token() !!}",
                profileid: $(this).attr('id')
            }
            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });
            $.ajax({
                url: `{{route('getDetailUser')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                    $('#detailData').html(data)
                    $('#profileDetail').modal({
                        show: true
                    }); 
                    swal.close()
                },
                error: function(err){
                    console.log(err)
                }
            })
        })
    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('state'))
            @if($errors->first('state'))
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @else
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @endif
        @endif
    })
</script>
@endpush