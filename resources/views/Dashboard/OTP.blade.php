@extends('main', [
    'class' => '',
    'elementActive' => 'otp'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / OTP List
            </div>
        </div>
        <div class="row">
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>OTP List</h5>
                            </div>
                            <div class="col text-right">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-custom" id="otpTable">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Otp Penerima</th>
                                <th scope="col">Otp Kode</th>
                                <th scope="col">Status</th>
                                <th scope="col">Status Code</th>
                                <th scope="col" class="text-right" width="25%">Date Created</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['data'] as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>{{MainController::ValidateDataNumber($item['otp_nohp'])}}</td>
                                        <td>{{$item['otp_kode']}}</td>
                                        <td>
                                            @foreach($data['status'] as $keys => $value)
                                                {{$value['otp_status'] === $item['otp_status'] ? $value['otptype'] : ''}}
                                            @endforeach
                                        </td>
                                        <td class="text-center">{{$item['otp_status']}}</td>
                                        <td class="text-right">
                                            {{\Carbon\Carbon::parse($item['otp_created_at'])}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#otpTable').DataTable();
    });
</script>
@endpush