@extends('main', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
    <div class="content">
        <div class="row">
            {{-- {{dd($data)}} --}}
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / <a href="{{route('Deposit')}}">Deposit</a> / Detail Deposit / {{$data['namaakun']}}
            </div>
            <div class="w-100 mb-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <form class="submit" method="POST" action="{{route('WrongTrfDeposit')}}">
                            @csrf
                            <input type="hidden" name="id" value="{{$data['iddeposit']}}" />
                            <div class="form-group">
                                <label>Deposit ID</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text pr-2">
                                            <i class="nc-icon nc-badge"></i>
                                        </span>
                                    </div>
                                    <input type="text" value="{{$data['iddeposit']}}" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Nama Akun</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text pr-2">
                                            <i class="nc-icon nc-single-02"></i>
                                        </span>
                                    </div>
                                    <input type="text" value="{{$data['namaakun']}}" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Nama Rekening</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text pr-2">
                                            <i class="nc-icon nc-credit-card"></i>
                                        </span>
                                    </div>
                                    <input type="text" value="{{$data['namarekening']}}" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Total Bayar</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text pr-2">Rp</span>
                                    </div>
                                    <input type="text" value="{{MainController::rupiah($data['totalbayar'])}}" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Expired</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text pr-2">
                                            <i class="nc-icon nc-calendar-60"></i>
                                        </span>
                                    </div>
                                    <input type="text" value="{{\Carbon\Carbon::create($data['expired'])}}" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text pr-2">
                                            <i class="nc-icon nc-calendar-60"></i>
                                        </span>
                                    </div>
                                    <input type="text" value="{{MainController::statusDeposit($data['status'])}}" class="form-control" disabled>
                                </div>
                            </div>

                            <?php
                            if(MainController::checkStatusDeposit($data['status'])){
                            ?>
                            <div class="form-group">
                                <label>Status</label>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text pr-2">Action</label>
                                </div>
                                <select class="custom-select" name="status" required>
                                    <option>Choose...</option>
                                    <option value="2" selected>Accept</option>
                                    <option value="6">Decline</option>
                                </select>
                            </div>
                            <?php  
                            }
                            ?>
                            @if($data['buktitransfer'])
                            <div class="form-group">
                                <label>Bukti Transfer</label>
                                <div class="form-group m-3 text-center">
                                    <img src="https://devsck.nenggala.id/files/{{$data['buktitransfer']}}" width="200px" data-toggle="modal" data-target="#exampleModal" />
                                </div>
                            </div>
                            @endif

                            <div class="form-group text-right">
                                <a href="{{route('Deposit')}}">
                                    <button type="button" class="btn btn-secondary">Kembali</button>
                                </a>
                                <?php
                                if(MainController::checkStatusDeposit($data['status'])){
                                ?>
                                <button type="button" class="btn btn-success submits">Simpan</button>
                                <?php 
                                }
                                ?>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <img src="https://devsck.nenggala.id/files/{{$data['buktitransfer']}}" />
          {{-- <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div> --}}
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript">
// function validateForm() {
//     var isValid = true;
//     $('form:input').each(function() {
//     if ( $(this).val() === '' ) {
//         alert('Some field are empty');
//         isValid = false;
//     } 
//     });
//     return isValid;
// }
    $(document).ready(function(){
        $('.submits').click(function(e){
            e.preventDefault();
            // const d = validateForm()
            // if(!d){
            //     return false
            // }
            swal({
                title: "Are you sure?",
                text: "Accept this Transaction",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal({
                        icon: "info",
                        text: 'Loading',
                        confirmButtonClass: "show-loading-icon",
                        cancelButtonClass: "show-loading-icon-delete",
                        buttons: false,
                        closeOnClickOutside: false
                    });
                    $('.submit').submit();
                } else {
                    swal("Okay");
                }
            });
        })
    })
</script>
@endpush