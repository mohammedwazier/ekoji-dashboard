@extends('main', [
    'class' => '',
    'elementActive' => 'cashflow'
])

@section('content')
<div class="content">
    <div class="row">
        <div class="col">
            <a href="{{route('Dashboard')}}">Dashboard</a> / Cashflow
        </div>

        <div class="w-100 mt-3"></div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-left icon-warning">
                                <i class="nc-icon nc-globe text-warning"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <div class="numbers">
                                <p class="card-category">Total Cashflow</p>
                                <p class="card-title">{{$data['success'] + $data['error']}}
                                    <p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        <i class="fa fa-refresh"></i> Update Now
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-left icon-warning">
                                <i class="nc-icon nc-globe text-warning"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <div class="numbers">
                                <p class="card-category">Cashflow Sukses</p>
                                <p class="card-title">{{$data['success']}}
                                    <p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        <i class="fa fa-refresh"></i> Update Now
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-left icon-warning">
                                <i class="nc-icon nc-globe text-warning"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <div class="numbers">
                                <p class="card-category">Cashflow Gagal</p>
                                <p class="card-title">{{$data['error']}}
                                    <p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        <i class="fa fa-refresh"></i> Update Now
                    </div>
                </div>
            </div>
        </div>
        <div class="w-100 mt-3"></div>

        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Refid</th>
                                <th scope="col">Kredit</th>
                                <th scope="col">Debet</th>
                                <th scope="col">Pemilik Cashflow</th>
                                {{-- <th scope="col">Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['list'] as $key => $item)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>
                                        <a href="" class="cashflow-detail" id="{{$item['cf_refid']}}">
                                            {{$item['cf_refid']}}
                                        </a>
                                    </td>
                                    <td>{{$item['kredit']}}</td>
                                    <td>{{$item['debet']}}</td>
                                    <td>{{$item['nama_profil']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="cashflowDetail" tabindex="-1" role="dialog" aria-labelledby="createProfileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5">
            <div class="modal-content p-3">
                <div id="detailData"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('popup'))
            {{-- @if($errors->first('state')) --}}
            setTimeout(function(){
                $("#{{$errors->first('id')}}").click();
            },750)
        @endif
        $('.cashflow-detail').on('click', function(e){
            e.preventDefault();
            var body = {
                _token: "{!! csrf_token() !!}",
                refid: $(this).attr('id')
            }
            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });
            $.ajax({
                url: `{{route('DetailCashflow')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                    // console.log(data)
                    $('#detailData').html(data)
                    $('#cashflowDetail').modal({
                        show: true
                    });
                    swal.close()
                },
                error: function(err){
                    console.log(err)
                }
            })
        })
    })
</script>
@endpush