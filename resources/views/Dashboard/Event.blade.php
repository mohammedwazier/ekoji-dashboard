@extends('main', [
    'class' => '',
    'elementActive' => 'event'
])

@section('content')
{{-- {{dd($errors)}} --}}
{{-- {{dd($data)}} --}}
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / OTP List
            </div>
        </div>
        <div class="row">
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Search
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('SearchEvent')}}">
                            @csrf
                            <div class="form-group">
                                <input type="search" class="form-control" name="search" placeholder="Search Anything" value="{{$errors->has('search') ? $errors->first('value') : ''}}" required />
                                @if($errors->has('search'))
                                    <span class="badge bg-green">{{$errors->first('message')}} <a href=""><i class="nc-icon nc-simple-remove"></i></a></span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>Event List</h5>
                            </div>
                            <div class="col text-right">
                            <div class="col text-right">
                                <button
                                class="btn btn-success mr-auto"
                                data-toggle="modal"
                                data-target="#createEvent"
                                data-backdrop="static"
                                >
                                    ADD +
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-custom" id="otpTable">
                            <thead>
                              <tr>
                                <th scope="col-1">No</th>
                                <th scope="col">Kode Event</th>
                                <th scope="col">Nama Event</th>
                                <th scope="col">Type</th>
                                <th scope="col">Nilai</th>
                                {{-- <th scope="col">Status</th> --}}
                                <th scope="col">Start Event</th>
                                <th scope="col">End Event</th>
                                <th scope="col">Created</th>
                                <th scope="col">Status</th>
                                <th scope="col">Detail</th>
                              </tr>
                            </thead>
                            <tbody>
                                @php
                                $iterator = $response['start'];
                                @endphp
                                @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">{{$iterator+1}}</th>
                                        <td>{{$value['event_kode']}}</td>
                                        <td>{{$value['event_nama']}}</td>
                                        <td>{{$value['event_type']}}</td>
                                        <td>{{$value['event_value']}}</td>
                                        <td>
                                            {{\Carbon\Carbon::parse($value['event_startdate'])}}
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::parse($value['event_enddate'])}}
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::parse($value['event_created'])}}
                                        </td>
                                        <td>
                                            {{MainController::StatusIsActive($value['event_isactive'])}}
                                        </td>
                                        <td>
                                            @if($value['event_isactive'] === 1)
                                            <a href="" class="_detail" id="{{$value['event_id']}}">
                                                Detail
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @php
                                    $iterator++;
                                    @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="10">
                                        <div>
                                            {{$data->links()}}
                                            <small>
                                                <i>Page : {{$response['current_page']}} | Total Data : {{$response['total']}}</i>
                                            </small>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createEvent" tabindex="-1" role="dialog" aria-labelledby="createEventTitle" aria-hidden="true">
        <form method="POST" action="{{route('CreateEvent')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Buat Event</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Event</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama Event" required>
                        </div>

                        <div class="form-group">
                            <label>Cover Event</label>
                            <div class="custom-file">
                                <input type="file" name="cover" class="custom-file-input" accept="image/*" id="eventID" img-target="eventCover">
                                <label class="custom-file-label eventCover" for="eventID">Cover Event</label>
                            </div>
                        </div>
                        <div class="form-group m-3 text-center" style="position: relative;">
                            <img src="" id="eventCover" width="700px" />
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="description" class="form-control editor">
                            </textarea>
                        </div>

                        @php
                        $listOpt1 = array(
                            array('id' => 1, 'keterangan' => 'Adding to User'),
                            array('id' => 2, 'keterangan' => 'Decrease from User'),
                            array('id' => 3, 'keterangan' => 'Decrease from Product')
                        );
                        @endphp

                        <div class="form-group">
                            <label>Metode Event</label>
                            <select name="method" class="form-control" required>
                                <option value="">Choose one</option>
                                <option value=""></option>
                                @foreach($listOpt1 as $key => $value)
                                <option value="{{$value['id']}}">{{$value['keterangan']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <label>Start Date</label>
                                <input type="date" name="startdate" class="form-control" required>

                                <label>End Date</label>
                                <input type="date" name="enddate" class="form-control" required>
                            </div>
                            <div class="col">
                                <label>Start Hour</label>
                                <input type="time" name="starthour" class="form-control">
                                <label>End Hour</label>
                                <input type="time" name="endhour" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Type Value Event</label>
                            <select class="form-control edit-type" name="pay_method">
                                <option value="nexus">Nexus</option>
                                <option value="persen">Persen</option>
                            </select>
                        </div>

                        <div class="form-group type" id="nexus">
                            <label>Nilai Nexus</label>
                            <input type="number" name="nilai" class="form-control nexus" placeholder="Nilai Nexus" required disabled>
                        </div>

                        <div class="form-group type" id="persen">
                            <label>Nilai Persen</label>
                            <input type="number" min="0" max="100" name="nilai" class="form-control persen" placeholder="Nilai Persen" required disabled>
                        </div>

                        <div class="form-group">
                            <label>Type Event</label>
                            <select name="event_type" class="form-control" required>
                                <option value="">Choose one</option>
                                <option value=""></option>
                                <option value="register">When Register</option>
                                <option value="product">When Buy Product</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Max used Event</label>
                            <small class="form-text text-muted">Maksimal pemakaian Event, total maksimal user memakai event tersebut</small>
                            <input type="number" value="0" name="used" class="form-control" placeholder="Maksilam pemakaian Event" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="createProfileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5">
            <div class="modal-content">
                <div id="target_modal_popup"></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
{{-- <script src="{{ asset('assets/js/ckeditor.js') }}"></script> --}}
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
{{-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> --}}
<script type="text/javascript">

    $(document).ready(function(){
        var allEditors = document.querySelectorAll('.editor');
        for (var i = 0; i < allEditors.length; ++i) {
            ClassicEditor.create(allEditors[i]);
        }
        $('.type').hide();

        if($('.edit-type').val() === 'nexus'){
            $('#nexus').show()
            $('.nexus').attr('disabled', false);
        }

        $("#eventID").change(function(){
            readURL(this, $(this).attr('img-target'))
        });

        $('.edit-type').on('change', function(){
            $('.type').hide();
            $('.type input').attr('disabled', true);
            if($(this).val() === 'nexus'){
                setTimeout(function(){
                    $('#nexus').show()
                    $('.nexus').attr('disabled', false);
                }, 200)
            }else{
                setTimeout(function(){
                    $('#persen').show()
                    $('.persen').attr('disabled', false);
                }, 200)
            }
        })

        $('._detail').on('click', function(e){
            e.preventDefault();
            var body = {
                _token: "{!! csrf_token() !!}",
                eventid: $(this).attr('id')
            }
            // console.log(body);
            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });
            $.ajax({
                url: `{{route('PopupEvent')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                    // console.log(data);
                    $('#target_modal_popup').html(data)
                    $('#modal_popup').modal({
                        show: true
                    }); 
                    swal.close()
                },
                error: function(err){
                    console.log(err)
                }
            })
        })
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('state'))
            @if($errors->first('state'))
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @else
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @endif
        @endif
    })
</script>
@endpush