@extends('main', [
    'class' => '',
    'elementActive' => 'transaksi'
])

@section('content')
@php
$data = $res['data'];
@endphp
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / Transaksi
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-globe text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Total Transaksi</p>
                                    <p class="card-title">
                                        {{count($data)}}
                                    <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-refresh"></i>
                            {{\Carbon\Carbon::now()}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-money-coins text-success"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Trx Pending/Proses</p>
                                    <p class="card-title">
                                        {{count($data) - $res['sukses']}} 
                                        <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-refresh"></i>
                            {{\Carbon\Carbon::now()}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-vector text-danger"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Transaksi Sukses</p>
                                    <p class="card-title">
                                        {{$res['sukses']}}
                                        <p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-refresh"></i>
                            {{\Carbon\Carbon::now()}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card card-stats">
                    <div class="card-header">
                        Search
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('SearchTransaction')}}">
                            @csrf
                            <div class="form-group">
                                <input type="search" class="form-control" name="search" placeholder="Search Anything" value="{{$errors->has('search') ? $errors->first('value') : ''}}" required />
                                @if($errors->has('search'))
                                    <span class="badge bg-green">{{$errors->first('message')}} <a href=""><i class="nc-icon nc-simple-remove"></i></a></span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>

            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>List Transaksi</h5>
                            </div>
                            <div class="col text-right">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table-responsive table-striped table-custom" style="overflow-y: hidden;">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">ID Trx</th>
                                <th scope="col">Tipe</th>
                                <th scope="col">Produk</th>
                                <th scope="col">Invoice</th>
                                <th scope="col">Code TRX Status</th>
                                <th scope="col">Code Inbox Status</th>
                                <th scope="col">Status Process</th>
                                {{-- <th scope="col">Judul</th> --}}
                                <th scope="col">Cashflow Refid</th>
                                {{-- <th scope="col">Profile ID</th> --}}
                                <th scope="col">Harga</th>
                                <th scope="col">Buyer User</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $item)
                                {{-- {{dd($item)}} --}}
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>
                                            <a href="">
                                                {{$item['trx_id']}}
                                            </a>
                                        </td>
                                        <td>{{$item['trx_tipe']}}</td>
                                        <td>
                                            @if($item['trx_produk_id'])
                                            <a href="{{route('ProdukPopup', ['id' => $item['trx_produk_id']])}}">
                                                {{$item['trx_produk_id']}}
                                            </a>
                                            @endif
                                        </td>
                                        <td>{{$item['trx_invoice']}}</td>
                                        <td>
                                            {{$item['trx_status']}}
                                        </td>
                                        <td>
                                            {{$item['ibx_status']}}
                                        </td>
                                        <td>
                                            {{MainController::compareStatusProcess($item['ibx_status'])}}
                                        </td>
                                        {{-- <td>{{$item['trx_judul']}}</td> --}}
                                        <td>
                                            <a href="{{route('PopupCashflow', ['id' => $item['trx_refid']])}}" target="_blank">
                                                {{$item['trx_refid']}}
                                            </a>
                                        </td>
                                        {{-- <td>
                                            <a href="{{route('DetailUser', ['id' => $item['trx_id_profile']])}}" target="_blank">
                                                {{$item['trx_id_profile']}}
                                            </a>
                                        </td> --}}
                                        <td>{{$item['trx_harga']}}</td>
                                        <td>
                                            <a href="{{route('DetailUser', ['id' => $item['trx_id_profile']])}}" target="_blank">
                                                {{$item['trx_namauser']}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>
        </div>
    </div>

    {{-- <div class="modal fade" id="profileDetail" tabindex="-1" role="dialog" aria-labelledby="createProfileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5">
            <div class="modal-content">
                <div id="detailData"></div>
            </div>
        </div>
    </div> --}}

@endsection

@push('scripts')
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script>
    $(document).ready(function(){
        // $('.profile-detail').on('click', function(e){
        //     e.preventDefault();
        //     var body = {
        //         _token: "{!! csrf_token() !!}",
        //         profileid: $(this).attr('id')
        //     }
        //     // console.log(body);
        //     swal({
        //         icon: "info",
        //         text: 'Loading',
        //         confirmButtonClass: "show-loading-icon",
        //         cancelButtonClass: "show-loading-icon-delete",
        //         buttons: false,
        //         closeOnClickOutside: false
        //     });
        //     $.ajax({
        //         url: `{{route('getDetailUser')}}`,
        //         type: 'POST',
        //         // dataType: 'json',
        //         headers: {
        //             'X-CSRF-TOKEN': `{!! csrf_token() !!}`
        //         },
        //         data: body,
        //         success: function(data){
        //             // console.log(textStatus)
        //             // console.log(data);
        //             $('#detailData').html(data)
        //             $('#profileDetail').modal({
        //                 show: true
        //             }); 
        //             swal.close()
        //             // location.reload();
        //         },
        //         error: function(err){
        //             console.log(err)
        //             // location.reload();
        //         }
        //     })
        // })
        // $('#NIK').on('keyup', function(){
        //     $.post('http://devsck.nenggala.id/api/v1/Register/validasi', {tipe: 'nik', value: $(this).val()}, function(data){
        //         if(data.code === 102 || data.code === 103){
        //             $('#NIK').addClass('is-invalid');
        //             $('#btnSucc').attr('disabled', true)
        //         }else if(data.code === 100){
        //             $('#NIK').removeClass('is-invalid').addClass('is-valid');
        //             $('#btnSucc').attr('disabled', false)
        //         }
        //     })
        // })
    })
</script>
@endpush