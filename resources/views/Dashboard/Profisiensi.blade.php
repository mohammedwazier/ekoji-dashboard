@extends('main', [
    'class' => '',
    'elementActive' => 'profisiensi'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / Profisiensi User List
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <div class="card card-stats">
                    <div class="card-header">
                        Search
                    </div>
                    <div class="card-body">
                        <form method="POST" id="searchForm" action="{{route('SearchPRofisiensi')}}">
                            @csrf
                            <div class="form-group">
                                <input type="search" class="form-control" name="search" placeholder="Search Anything" value="{{$errors->has('search') ? $errors->first('value') : ''}}" required />
                                @if($errors->has('search'))
                                    <span class="badge bg-green">{{$errors->first('message')}} <a href=""><i class="nc-icon nc-simple-remove"></i></a></span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>Profisiensi</h5>
                            </div>
                            <div class="col text-right">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-custom" id="otpTable">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Produk</th>
                                <th scope="col">Pemateri</th>
                                <th scope="col">Username</th>
                                <th scope="col">Password</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $value)
                                <tr id="prftable_{{$key}}">
                                    <td>{{$key+1}}</td>
                                    <td>
                                        <a href="{{route('ProdukPopup', ['id' => $value['produk_id']])}}" target="_blank">{{$value['produk_namaProduk']}}</a>
                                    </td>
                                    <td>
                                        <a href="{{route('DetailUser', ['id' => $value['pemateriid']])}}" target="_blank">
                                        {{$value['pematerinama']}}
                                        </a>
                                    </td>
                                    <td>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i onclick="copyToClipboard('#uname_{{$key}}')" style="cursor: pointer;" class="nc-icon nc-single-copy-04"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{$value['prf_username']}}" readonly aria-label="Username" aria-describedby="basic-addon1" id="uname_{{$key}}">
                                        </div>
                                    </td>
                                    <td>
                                    <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i onclick="copyToClipboard('#pswd_{{$key}}')" style="cursor: pointer;" class="nc-icon nc-single-copy-04"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{$value['prf_password']}}" readonly aria-label="Username" aria-describedby="basic-addon1" id="pswd_{{$key}}">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input done_create" data-target="{{$key}}" id="customSwitch1_{{$key}}">
                                            <label class="custom-control-label" for="customSwitch1_{{$key}}">Sudah Dibuat</label>
                                            <input type="hidden" value="{{json_encode($value, true)}}" id="{{$key}}">
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
{{-- <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> --}}
<script type="text/javascript">
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).val()).select();
        document.execCommand("copy");
        $temp.remove();
        $.notify({
            // options
            message: `<b>${$(element).val()}</b> Success Copied to Clipboard`
        },{
            // settings
            type: 'success'
        });
    }
    $(document).ready(function(){
        $('.done_create').on('change', function(){
            let target = $(this).attr('data-target');
            let body = JSON.parse($('#'+target).val());
            body = {
                _token: "{!! csrf_token() !!}",
                ...body
            }
            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });
            $.ajax({
                url: `{{route('CreateUserProfisiensi')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(response){
                    swal.close()
                    $('#prftable_'+target).remove();
                    setTimeout(function(){
                        if(response.state){
                            swal({
                                text: response.message,
                                icon: 'success'
                            })
                        }else{
                            swal({
                                text: response.message,
                                icon: 'error'
                            })
                        }
                    }, 200)
                },
                error: function(err){
                    console.log(err)
                    swal.close()
                    alert('Failed to Get Produk');
                }
            })
            // console.log(data);
            // console.log(target)

        })
    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('state'))
            @if($errors->first('state'))
                swal({
                    text: "{{session('errors')->first('message')}}",
                    icon: 'success'
                })
            @else
                swal({
                    text: "{{session('errors')->first('message')}}",
                    icon: 'success'
                })
            @endif
        @endif
    })
</script>
@endpush