@extends('main', [
    'class' => '',
    'elementActive' => 'produk'
])

@section('content')
{{-- {{dd($data)}} --}}
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / Produk
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-globe text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Total Produk</p>
                                    <p class="card-title">{{$data['static']['produk']}}
                                        </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-refresh"></i> 
                            {{\Carbon\Carbon::now()}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-money-coins text-success"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Kategori Produk</p>
                                    <p class="card-title">{{$data['static']['kategori']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-refresh"></i>
                            {{\Carbon\Carbon::now()}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-vector text-danger"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Produk Aktif</p>
                                    <p class="card-title">{{$data['static']['produkaktif']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            {{-- <i class="fa fa-clock-o"></i> In the last hour --}}
                            <i class="fa fa-refresh"></i> 
                            {{\Carbon\Carbon::now()}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big icon-warning">
                                    <i class="nc-icon nc-globe text-warning"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Temp Deleted</p>
                                    <p class="card-title">{{$data['static']['tempdelete']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-refresh"></i> 
                            {{\Carbon\Carbon::now()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Search
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('SearchProductPage')}}">
                            @csrf
                            <div class="form-group">
                                <input type="search" class="form-control" name="search" placeholder="Search Anything" value="{{$errors->has('search') ? $errors->first('value') : ''}}" required />
                                @if($errors->has('search'))
                                    <span class="badge bg-green">{{$errors->first('message')}} <a href="{{route('Produk')}}"><i class="nc-icon nc-simple-remove"></i></a></span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>
            <div class="col-lg-5 col-md-5 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>Kategori Produk List</h5>
                            </div>
                            <div class="col text-right">
                                <button
                                class="btn btn-success mr-auto"
                                data-toggle="modal"
                                data-target="#kategoriModal"
                                data-backdrop="static"
                                >
                                    ADD +
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Kategori</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['kategori'] as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>{{$item['nama']}}</td>
                                        <td class="text-center">
                                            <a href="{{route('DetailKategori', ['id' => $item['groupid']])}}">
                                            Detail
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>Produk List</h5>
                            </div>
                            <div class="col text-right">
                                <button
                                class="btn btn-success mr-auto"
                                data-toggle="modal"
                                data-target="#produkModal"
                                data-backdrop="static"
                                >
                                    ADD +
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive" style="overflow-y:hidden!important;">
                            <table class="table" id="produkTable">
                                <thead>
                                  <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Produk</th>
                                    <th scope="col">Kode Produk</th>
                                    <th scope="col">Kategori</th>
                                    <th scope="col">Harga</th>
                                    <th scope="col">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data['produk_paginate'] as $key => $item)
                                    {{-- {{dd($item['produkid'])}} --}}
                                        <tr>
                                            <th scope="row">{{$key+1}}</th>
                                            <td>
                                                <a href="#" class="produk_detail" id="{{$item['produkid']}}">
                                                    {{$item['nama']}}
                                                </a>
                                            </td>
                                            <td>{{$item['kodeproduk']}}</td>
                                            <td>
                                                {{$item['groupnama']}}
                                            </td>
                                            <td>{{substr(MainController::rupiah($item['harga']), 3)}} Nexus</td>
                                            <td class="text-center">
                                                <a href="#" class="produk_detail" id="{{$item['produkid']}}">
                                                    Detail
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            <div>
                                                {{$data['produk_paginate']->links()}}
                                                <small>
                                                    <i>Page : {{$data['current_page']}} | Total Data : {{$data['total']}}</i>
                                                </small>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="kategoriModal" tabindex="-1" role="dialog" aria-labelledby="kategoriModalTitle" aria-hidden="true">
        <form method="POST" action="{{route('CreateKategori')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Buat Kategori Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama Kategori" required>
                        </div>
                        <label>Gambar Kategori</label>
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="kategoriPic" accept="image/*" required>
                            <label class="custom-file-label kategoriImg" for="kategoriPic">Pilih Gambar Kategori</label>
                        </div>
                        <div class="form-group m-3 text-center">
                            <img src="" id="kategoriImg" width="300px" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="produkModal" tabindex="-1" role="dialog" aria-labelledby="produkModalTitle" aria-hidden="true">
        <form method="POST" action="{{route('CreateProduk')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Buat Produk Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Produk</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama Produk" required>
                        </div>

                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control editor"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Kategori Produk</label>
                            <select class="custom-select mr-sm-2" id="changeProduk" name="tipeproduk" required>
                                <option selected></option>
                                @foreach($data['kategori'] as $key => $item)
                                <option value="{{$item['groupid']}}" id="val{{$key}}" namaid="{{strtolower($item['nama'])}}">{{$item['nama']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Pemateri</label>
                            <select class="custom-select mr-sm-2" name="pemateri" required>
                                <option selected></option>
                                @foreach($data['user'] as $key => $item)
                                <option value="{{$item['id']}}" id="val{{$key}}" namaid="{{strtolower($item['nama'])}}">{{$item['nama']}}</option>
                                @endforeach
                            </select>
                        </div>

                        {{-- Seleksi jika 1 2 3 4 --}}

                        <div id="e-book" class="produks">
                            <div class="form-group">
                                <label>Produk E-book</label>
                                <div class="custom-file">
                                    <input type="file" name="certificate" class="custom-file-input ebooks" accept="application/pdf" required>
                                    <label class="custom-file-label">Pilih Ebook</label>
                                </div>
                            </div>
                        </div>

                        <div id="e-presentasi" class="produks">
                            <div class="form-group">
                                <label>Produk E-Presentasi</label>
                                <div class="custom-file">
                                    <input type="file" name="certificate" class="custom-file-input presentasy" accept="application/vnd.ms-powerpoint" required>
                                    <label class="custom-file-label">Pilih EPresentasi</label>
                                </div>
                            </div>
                        </div>

                        <div id="e-certificate" class="produks">
                            <div class="form-group">
                                <label>Produk Certificate</label>
                                <div class="custom-file">
                                    <input type="file" name="certificate" class="custom-file-input" accept="image/*" id="produkPic" img-target="produkImg" required>
                                    <label class="custom-file-label produkImg" for="produkPic">Pilih Produk Certificate</label>
                                </div>
                            </div>
                            <div class="form-group m-3 text-center" style="position: relative;">
                                <div id="parentCanvas">
                                    <div class="dragContent clickedContent nama" id="MainName" style="font-size:13px;" data-type="name">
                                        The Brown Fox Jumps Over The Lazy Dog
                                    </div>
                                    <div class="dragContent clickedContent" id="nomor_surat" style="font-size: 12px;" data-type="nomor">
                                        NOMOR : PREI/SEM/KODE_PRODUK/{{MainController::romanize(\Carbon\Carbon::now()->month)}}/{{\Carbon\Carbon::now()->year}}/0001
                                    </div>
                                    <img src="" id="produkImg" width="700px" />
                                </div>
                                <input type="hidden" id="data-image" name="data_image" class="data-image">
                                <input type="hidden" id="data-name" name="data_name">
                                <input type="hidden" id="data-nomor" name="data_nomor">
                            </div>

                            <div class="w-100"></div>
                            <div class="form-group m-3">
                                <label>Option Nama</label>
                                <select class="form-control fontSize" form-target="MainName">
                                    @for($idx = 13; $idx < 38; $idx++){
                                        <option {{$idx == 13 ? 'selected' : ''}} value="{{$idx}}">{{$idx}}</option>
                                    @endfor
                                </select>
                                <label>Warna</label>
                                <select class="form-control colorChange" form-target="MainNameEditPlace">
                                    <option selected value="b">Black</option>
                                    <option value="w">White</option>
                                </select>
                                <button type="button" class="center-y btn btn-success" form-target="MainName">Center Y</button>
                                <button type="button" class="center-x btn btn-success" form-target="MainName">Center X</button>
                            </div>
                            <div class="form-group m-3">
                                <label>Option Nomor</label>
                                <select class="form-control fontSize" form-target="nomor_surat">
                                    <option value="8">8</option>
                                    <option value="10">10</option>
                                    <option selected value="12">12</option>
                                    <option value="14">14</option>
                                    <option value="16">16</option>
                                    <option value="32">32</option>
                                </select>
                                <label>Warna</label>
                                <select class="form-control colorChange" form-target="MainNameEditPlace">
                                    <option selected value="b">Black</option>
                                    <option value="w">White</option>
                                </select>
                                <button type="button" class="center-y btn btn-success" form-target="nomor_surat">Center Y</button>
                                <button type="button" class="center-x btn btn-success" form-target="nomor_surat">Center X</button>
                            </div>

                            <div class="form-group">
                                <label>Mulai</label>
                                <input type="date" name="startDate" class="form-control startDate changeDate" id="startDate">
                            </div>
                            <div class="form-group">
                                <label>Akhir</label>
                                <input type="date" name="endDate"  class="form-control endDate changeDate" id="endDate">
                            </div>

                            <div class="form-group">
                                <label>Jam Mulai</label>
                                <input type="time" name="startTime" class="form-control startTime changeDate" id="startTime">
                            </div>
                            <div class="form-group">
                                <label>Jam Akhir</label>
                                <input type="time" name="endTime" class="form-control endTime changeDate" id="endTime">
                            </div>

                        </div>

                        <div id="e-profisiensi" class="produks">
                            <div class="form-group">
                                <label>Mulai Profisiensi</label>
                                <input type="date" name="startDate" class="form-control startDate changeDate" id="startDate">
                            </div>
                            <div class="form-group">
                                <label>Akhir Profisiensi</label>
                                <input type="date" name="endDate"  class="form-control endDate changeDate" id="endDate">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Gambar Cover</label>
                            <div class="custom-file">
                                <input type="file" name="cover" class="custom-file-input" id="produkCoverPic" accept="image/*" required>
                                <label class="custom-file-label produkCoverImg" for="produkCoverPic">Pilih Cover Produk</label>
                            </div>
                        </div>
                        <div class="form-group m-3 text-center">
                            <img src="" id="produkCoverImg" width="300px" />
                        </div>

                        <div class="form-group">
                            <label>Link</label>
                            <input type="text" name="link" class="form-control" placeholder="Link">
                        </div>

                        <div class="form-group">
                            <label>Harga <sub>(Nexus)</sub></label>
                            {{-- <input type="range" class="form-control-range" name="rangeInput" min="0" max="100" onchange="updateTextInput(this.value);"> --}}
                            <input type="number" class="form-control" min="1" value="1" max="9999999999" name="harga" id="textInput" value="">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="produkDetail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5" style="max-width: 728px!important">
            <div class="modal-content">
                <div id="detailData"></div>
                <div id="placementDetailForm"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
{{-- <script src="{{ asset('assets/js/ckeditor.js') }}"></script> --}}
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{-- <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> --}}
<script type="text/javascript" src="{{asset('assets/js/PlacementCertificate.js')}}"></script>
<script type="text/javascript">

    var allEditors = document.querySelectorAll('.editor');
    for (var i = 0; i < allEditors.length; ++i) {
        ClassicEditor.create(allEditors[i]);
    }
    $(document).ready(function(){
        $('#placementDetailForm').hide()
        @if(!empty($data['popupData']))
             var body = {
                _token: "{!! csrf_token() !!}",
                produkid: `{{$data['id']}}`
            }

            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });

            $.ajax({
                url: `{{route('DetailProduk')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                    $('#detailData').show().html(data)
                    $('#placementDetailForm').hide()
                    // $('#detailData').show()
                    $('#produkDetail').modal({
                        show: true
                    });
                    swal.close()
                },
                error: function(err){
                    console.log(err)
                    swal.close()
                    alert('Failed to Get Produk');
                }
            })
        @endif
        $('.produk_detail').on('click', function(e){
            e.preventDefault();

            var body = {
                _token: "{!! csrf_token() !!}",
                produkid: $(this).attr('id')
            }

            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });

            $.ajax({
                url: `{{route('DetailProduk')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                    $('#detailData').show().html(data)
                    $('#placementDetailForm').hide()
                    $('#produkDetail').modal({
                        show: true
                    });
                    swal.close()
                },
                error: function(err){
                    console.log(err)
                    swal.close()
                    alert('Failed to Get Produk');
                }
            })
        })

        $('.changeDate').on('change', function(){
            var date = new Date();
            var currentDate = date.toISOString().slice(0,10);
            var curr = new Date(currentDate);
            var addDate = new Date(date.setDate(date.getDate() + 1));
            var valaddDate = addDate.toISOString().slice(0,10);
            var id = $(this).attr('id');

            var idStart = 'startDate';
            var idEnd = 'endDate';

            if(id === 'startDate'){
                var dateStart = new Date($(this).val());
                if(dateStart < curr){
                    $(this).val(currentDate);
                    if(!$(`#${idEnd}`).val()) $(`#${idEnd}`).val(valaddDate)
                }else{
                    if(!$(`#${idEnd}`).val()){
                        dateStart = new Date(dateStart);
                        // var dateEnd = new Date(date.setDate(dateStart.getDate() + 1))
                        var dateEnd = new Date(dateStart);
                        var valdateEnd = dateEnd.toISOString().slice(0,10);
                        $(`#${idEnd}`).val(valdateEnd)
                    }else{
                        if(new Date($(this).val()) >= new Date($(`#${idEnd}`).val())){
                            dateStart = new Date(dateStart);
                            // var dateEnd = new Date(date.setDate(dateStart.getDate() + 1))
                            var dateEnd = new Date(dateStart);
                            var valdateEnd = dateEnd.toISOString().slice(0,10);
                            $(`#${idEnd}`).val(valdateEnd)
                        }
                    }
                }
            }else{
                var dateEnd = new Date($(this).val());

                if(!$(`#${idStart}`).val()){
                    if(dateEnd <= curr){
                        $(this).val(valaddDate);
                        $(`#${idStart}`).val(currentDate)
                    }
                }else{
                    var dateStart = new Date($(`#${idStart}`).val());
                    if(dateEnd <= dateStart){
                        dateEnd = new Date(dateStart.setDate(dateStart.getDate() + 0));
                        dateEnd = dateEnd.toISOString().slice(0,10);
                        $(this).val(dateEnd);
                    }
                }
            }
        })

        $('.ebooks, .presentasy').on('change', function(e) {
            var fileName = e.target.files[0].name;
            $(this).closest('div').find('label').html(fileName);
        })


        $('.produks :input').attr('disabled', true);
        $('.produks').hide();

        $('#changeProduk').on('change', function(){
            var id = $("option:selected", this).attr('namaid')
            $('.produks :input').attr('disabled', true);
            $('.produks').hide();
            setTimeout(function(){
                $(`#${id}`).show();
                $(`#${id} :input`).attr('disabled', false)
            },100)
        })
    })

    $("#kategoriPic").change(function(){
        readURL(this, 'kategoriImg', 1, 1);
    });

    $("#produkPic").change(function(){
        readURL(this, $(this).attr('img-target'), 0, 0)
        .then(updateLoad)
    });

    $("#produkCoverPic").change(function(){
        readURL(this, 'produkCoverImg', 0, 0);
    });


</script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('state'))
            @if($errors->first('state'))
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @else
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @endif
        @endif
    })
</script>
@endpush