@extends('main', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
@php
$image = !empty($data['photo']) ? $data['photolink'] : asset('assets/img/faces/ayo-ogunseinde-2.jpg');
@endphp
<div class="content">
    <div class="row">
        <div class="col">
            <a href="{{route('Dashboard')}}">Dashboard</a> / <a href="{{route('User')}}">Profile</a> / Detail / {{$data['nama']}}
        </div>
        <div class="w-100 mt-3"></div>
        <div class="col">
        	<div class="card">
        		<div class="card-body">
        			<div class="row">
        				<div class="col-md-4 justify-content-center align-items-center p-3">
                            <div class="row">
                                <div class="col">
                                    <div class="profilePhoto" style="background: url('{{$image}}') no-repeat;  background-size: cover;"></div>
                                </div>
                                <div class="w-100 mt-3"></div>
                                <div class="col text-center">
                                    <span class="tag-role bg-green1">Role</span>
                                    <span class="tag-role bg-green1">{{$data['role']}}</span>
                                </div>
                            </div>
        				</div>
        				<div class="col d-flex align-items-center {{-- justify-content-center --}}">
                            <div>
                            <h2>{{$data['nama']}}</h2>
                            <p>
                                ID : {{$data['id']}}
                                <br />
                                NIK: {{$data['nik']}}
                                <br />
                                Alamat : {{$data['alamat']}}
                                <br />
                                Tempat Lahir : {{$data['tempat_lahir']}}
                                <br />
                                Tanggal Lahir : {{$data['tanggal_lahir']}}
                                <br />
                                Username : {{$data['username']}}
                                <br />
                                NoHP : {{$data['nohp']}}
                                <br />
                                Saldo : {{MainController::rupiah($data['saldo'])}}
                                <br />
                                Saldo Nexus : {{MainController::rupiah($data['saldo_nexus'])}}
                            </p>
                            </div>

        				</div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
    
</div>


</div> --}}
@endsection

@push('scripts')

@endpush