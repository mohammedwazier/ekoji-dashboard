@extends('main')

@section('content')
@php
$kategori = $data['kategoriList'];
@endphp
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / <a href="{{route('Produk')}}">Produk</a> / Detail Produk / {{$data['namaproduk']}}
            </div>
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header text-right">
                        <button onclick="swAlert(event, 'Once deleted, you will not be able to recover this file!', 'link')" href="{{route('DeleteProduk', ['id' => $data['produkid']])}}" class="btn btn-danger">Hapus</button>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('UpdateProduk')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="produkid" value="{{$data['produkid']}}">
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <input type="text" name="name" value="{{$data['namaproduk']}}" class="form-control" placeholder="Nama Kategori">
                            </div>
        
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="keterangan" class="form-control editor">
                                    {!! $data['keteranganproduk'] !!}
                                </textarea>
                            </div>
        
                            <div class="form-group">
                                <label>Kategori Produk</label>
                                <select class="custom-select mr-sm-2" name="tipeproduk">
                                    @foreach($kategori as $key => $item)
                                        <option value="{{$item['groupid']}}" {{$data['groupid'] == $item['groupid'] ? 'selected' : ''}}>{{$item['nama']}}</option>
                                    @endforeach
                                </select>
                            </div>
        
                                {{-- Seleksi jika 1 2 3 4 --}}
        
                            <div class="form-group">
                                <label>Gambar Produk {{-- Sertifikat --}} </label>
                                <div class="custom-file">
                                    <input type="file" value="" name="certificate" class="custom-file-input" id="produkPic" accept="image/*">
                                    <label class="custom-file-label produkImg" for="produkPic">Pilih Gambar Produk</label>
                                </div>
                            </div>
                            <div class="form-group m-3 text-center">
                                
                                <img src="{{ENV::image($data['certificate'])}}" id="produkImg" width="300px" />
                            </div>
        
                            <div class="form-group">
                                <label>Gambar Cover</label>
                                <div class="custom-file">
                                    <input type="file" name="cover" class="custom-file-input" id="produkCoverPic" accept="image/*">
                                    <label class="custom-file-label produkCoverImg" for="produkCoverPic">Pilih Cover Produk</label>
                                </div>
                            </div>
                            <div class="form-group m-3 text-center">
                                <img src="{{ENV::image($data['produkcover'])}}" id="produkCoverImg" width="300px" />
                            </div>
        
                            <div class="form-group">
                                <label>Link</label>
                                <input type="text" name="link" value="{{$data['linkproduk']}}" class="form-control" placeholder="Link">
                            </div>
        
                            <div class="form-group">
                                <label>Harga <sub>(Nexus)</sub></label>
                                {{-- <input type="range" class="form-control-range" name="rangeInput" min="0" max="100" onchange="updateTextInput(this.value);"> --}}
                                <input type="number" value="{{$data['produkharga']}}" class="form-control" min="0" value="1" max="9999" name="harga" id="textInput" value="">
                            </div>
                            <div class="form-group text-right">
                                <a href="{{route('Produk')}}">
                                    <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </a>
                                <button type="submit" class="btn btn-success">Ubah</button>
                            </div>
                                    
                        </div>
                    </form>
                        
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('assets/js/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script type="text/javascript">
    var allEditors = document.querySelectorAll('.editor');
    for (var i = 0; i < allEditors.length; ++i) {
        ClassicEditor.create(allEditors[i]);
    }
    
    $("#kategoriPic").change(function(){
        readURL(this, 'kategoriImg', 1, 1);
    });

    $("#produkPic").change(function(){
        readURL(this, 'produkImg', 65, 46);
    });

    $("#produkCoverPic").change(function(){
        readURL(this, 'produkCoverImg', 0, 0);
    });

    function swAlert(e, text, type){
        e.preventDefault();
            swal({
                title: "Are you sure?",
                text: text,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal({
                        icon: "info",
                        text: 'Loading',
                        confirmButtonClass: "show-loading-icon",
                        cancelButtonClass: "show-loading-icon-delete",
                        buttons: false,
                        closeOnClickOutside: false

                    });
                    if(type === 'link'){
                        location.href = $(e.target).attr('href')
                    }else{
                        // $(e.target).submit();
                    }
                } else {
                    swal("OK!");
                }
        });
    }
</script>
@endpush