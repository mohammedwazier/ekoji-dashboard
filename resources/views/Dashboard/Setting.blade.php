@extends('main', [
    'class' => '',
    'elementActive' => 'setting'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <a href="{{route('Dashboard')}}">Dashboard</a> / Setting List
            </div>
        </div>
        <div class="row">
            <div class="w-100 mt-3"></div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5>Setting List</h5>
                            </div>
                            <div class="col text-right">
                                <button
                                class="btn btn-success mr-auto"
                                data-toggle="modal"
                                data-target="#CreateSetting"
                                data-backdrop="static"
                                >
                                    ADD +
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-custom" id="tableData">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Tipe Value</th>
                                <th scope="col">Value</th>
                                <th scope="col">Status</th>
                                <th scope="col" class="text-right" {{-- width="10%" --}}>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $item)
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>{{$item['st_kode']}}</td>
                                        <td>{{$item['st_typevalue']}}</td>
                                        <td>{{strlen($item['st_value']) > 100 ? substr($item['st_value'], 1, 100)." . . . ." : $item['st_value']}}</td>
                                        <td>{{intval($item['st_isactive']) === 1 ? 'Active' : 'Not Active'}}</td>
                                        <td>
                                            {{-- Detail --}}
                                            <a href="" class="detail-data" id="{{$item['st_kode']}}">
                                                Detail
                                            </a>
                                            <a href="{{route('DeleteSetting', ['kode' => $item['st_kode']])}}" onclick="return confirm('Are you sure to delete this Setting')">
                                                Hapus
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="CreateSetting" tabindex="-1" role="dialog" aria-labelledby="CreateSettingTitle" aria-hidden="true">
        <form method="POST" action="{{route('CreateSetting')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Buat Setting Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group mb-3">
                            <label>Kode</label>
                            <input type="text" name="kode" class="form-control" placeholder="Kode. Cont: st_otp" required>
                        </div>

                        <div class="form-group mb-3">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" placeholder="Description" required></textarea>
                        </div>

                        <div class="form-group mb-3">
                            <label>Kode</label>
                            {{-- <input type="text" name="kode" class="form-control" placeholder="Kode. Cont: st_otp" required> --}}
                            <select name="typevalue" class="form-control" required>
                                <option value="">--PILIH--</option>
                                <option value="list">List</option>
                                <option value="integer">Integer</option>
                                <option value="float">Float</option>
                                <option value="boolean">Boolean</option>
                                <option value="string">String</option>
                                <option value="object">Object</option>
                                <option value="date">Date</option>
                                <option value="time">Time</option>
                            </select>
                        </div>

                        <div class="form-group mb-3">
                            <label>Value</label>
                            <textarea name="value" class="form-control" placeholder="Value Data" required></textarea>
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input
                                type="checkbox"
                                class="custom-control-input changed"
                                id="customSwitches"
                                name="active"
                                >
                                <label class="custom-control-label" for="customSwitches">Active</label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="btnSucc">Tambah</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="popupDetail" tabindex="-1" role="dialog" aria-labelledby="createProfileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mt-5 mb-5">
            <div class="modal-content">
                <div id="detailData"></div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('assets/js/CustomMain.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tableData').DataTable();

        $('.detail-data').on('click', function(e){
            e.preventDefault();
            var body = {
                _token: "{!! csrf_token() !!}",
                kode: $(this).attr('id')
            }
            swal({
                icon: "info",
                text: 'Loading',
                confirmButtonClass: "show-loading-icon",
                cancelButtonClass: "show-loading-icon-delete",
                buttons: false,
                closeOnClickOutside: false
            });
            $.ajax({
                url: `{{route('PopupSetting')}}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': `{!! csrf_token() !!}`
                },
                data: body,
                success: function(data){
                    $('#detailData').html(data)
                    $('#popupDetail').modal({
                        show: true
                    });
                    swal.close()
                },
                error: function(err){
                    console.log(err)
                }
            })
        })
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        @if($errors->has('state'))
            @if($errors->first('state'))
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @else
                swal({
                    text: "{{$errors->first('message')}}",
                    icon: 'success'
                })
            @endif
        @endif
    })
</script>
@endpush