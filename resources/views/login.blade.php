<!DOCTYPE html>
<html lang="en">
<head>
	<title>Prexus Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/animate/animate.css')}}">
<!--===============================================================================================-->	
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->	
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
                <form class="login100-form validate-form" method="POST" action="{{route('LoginProcess')}}">
                    @csrf
					<span class="login100-form-title p-b-43">
						Login to Prexus Dashboard
					</span>

					<div class="wrap-input100 validate-input" {{-- data-validate = "Valid email is required: ex@abc.xyz" --}} required>
						<input class="input100" type="text" name="username">
						<span class="focus-input100"></span>
						<span class="label-input100">Username</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required" required>
						<input class="input100" type="password" name="password">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>

                    <div class="form-group ml-4">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="saveCred">
                            <label class="form-check-label" for="saveCred">Save Me!</label>
                        </div>
                    </div>



					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>

            <div class="login100-more" style="background-image: url('{{asset('assets/images/bg-01.jpg')}}');">
				</div>
			</div>
		</div>
	</div>
<!--===============================================================================================-->
    <script src="{{asset('assets/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assets/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assets/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assets/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assets/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assets/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		@if($errors->has('error'))
    			swal({
    				text: "{{session('errors')->first('error')}}",
    				icon: 'error'
    			})
    		@endif

            // if(){

            // }

            let email, password, statusSave = false;

            let storage = window.localStorage;

            if(storage.getItem('save')){
                statusSave = true;
                let data = JSON.parse(storage.getItem('save'));
                let input = $('input.input100');
                for(let idx = 0; idx < input.length; idx++){
                    let inputData = input[idx];
                    let nameAttr = $(inputData).attr('name');
                    $(inputData).focus().val(data[nameAttr]);
                }
                $('#saveCred').prop('checked', statusSave)
            }

            $('form').on('submit', function(e){
                let data = JSON.parse(storage.getItem('save'));
                let dataInput = $(this).serializeArray();
                delete dataInput[0];
                dataInput.map(d => {
                    if(d.value !== data[d.name]){
                        data[d.name] = d.value
                    }
                })
                storage.setItem('save', JSON.stringify(data));
            })

            $('#saveCred').on('change', function(){
                let ini = $(this);
                let value = ini.prop('checked');
                if(statusSave === false && value === true){
                    let input = $('input.input100');
                    let data = {};
                    for(let idx = 0; idx < input.length; idx++){
                        if($(input[idx]).val().length === 0){
                            $(input[idx]).focus();
                            ini.prop('checked', false)
                            break;
                        }else{
                            let inputData = $(input[idx]);
                            let nameData = inputData.attr('name');
                            let valData = inputData.val();
                            data[nameData] = valData
                        }
                    }
                    storage.setItem('save', JSON.stringify(data));
                }else if(statusSave === true && value === false){
                    storage.removeItem('save');
                }
            })

    	})
    </script>
</body>
</html>