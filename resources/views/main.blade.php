<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Ekoji Dashboard
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  {{-- <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" /> --}}
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --}}
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet" />
  <link href="{{asset('assets/css/paper-dashboard.css')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  {{-- <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" /> --}}

  {{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> --}}


  {{-- <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> --}}
  {{-- <lonk href="{{asset('assets/datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" /> --}}
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.17/dist/css/bootstrap-select.min.css">
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <div class="logo">
          <div class="simple-text logo-normal">
            Ekoji
          </div>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">

          <li class="{{$elementActive === 'homepage' ? 'active' : ''}}">
            <a href="{{route('Dashboard')}}">
              <i class="nc-icon nc-spaceship"></i>
              <p>Homepage</p>
            </a>
          </li>

          <li class="{{$elementActive === 'deposit' ? 'active' : ''}}">
            <a href="{{route('Deposit')}}">
              <i class="nc-icon nc-tile-56"></i>
              <p>Deposit <label class="countNumber">{{MainController::getNotifDeposit()['deposit_notif'] ? MainController::getNotifDeposit()['deposit_notif'] : 0}}</label></p>
            </a>
          </li>

          <li class="{{$elementActive === 'produk' ? 'active' : ''}}">
            <a href="{{route('Produk')}}">
              <i class="nc-icon nc-air-baloon"></i>
              <p>Produk</p>
            </a>
          </li>

          <li class="{{$elementActive === 'profile' ? 'active' : ''}}">
            <a href="{{route('User')}}">
              <i class="nc-icon nc-atom"></i>
              <p>User</p>
            </a>
          </li>

          <li class="{{$elementActive === 'transaksi' ? 'active' : ''}}">
            <a href="{{route('Transaksi')}}">
              <i class="nc-icon nc-credit-card"></i>
              <p>Transaksi</p>
            </a>
          </li>

          <li class="{{$elementActive === 'otp' ? 'active' : ''}}">
            <a href="{{route('Otp')}}">
              <i class="nc-icon nc-mobile"></i>
              <p>List OTP</p>
            </a>
          </li>

          <li class="{{$elementActive === 'cashflow' ? 'active' : ''}}">
            <a href="{{route('Cashflow')}}">
              <i class="nc-icon nc-chart-pie-36"></i>
              <p>Cashflow</p>
            </a>
          </li>

          <li class="{{$elementActive === 'event' ? 'active' : ''}}">
            <a href="{{route('Event')}}">
              <i class="nc-icon nc-trophy"></i>
              <p>Event</p>
            </a>
          </li>


          <li class="{{$elementActive === 'profisiensi' ? 'active' : ''}}">
            <a href="{{route('Profisiensi')}}">
              <i class="nc-icon nc-badge"></i>
              <p>Profisiensi User</p>
            </a>
          </li>

          <li class="{{$elementActive === 'setting' ? 'active' : ''}}">
            <a href="{{route('Setting')}}">
              <i class="nc-icon nc-touch-id"></i>
              <p>Settings</p>
            </a>
          </li>

          <li class="{{$elementActive === 'userguide' ? 'active' : ''}}">
            <a href="{{route('Userguide')}}">
              <i class="nc-icon nc-caps-small"></i>
              <p>Userguide</p>
            </a>
          </li>

          {{-- <li class="{{$elementActive === 'internal' ? 'active' : ''}}">
            <a href="{{route('InternalAccount')}}">
              <i class="nc-icon nc-globe"></i>
              <p>Internal Account</p>
            </a>
          </li> --}}

          <li>
            <a href="{{route('phpinfo')}}" target="_blank">
              <i class="nc-icon nc-globe"></i>
              <p>PHP Info</p>
            </a>
          </li>

          <hr />

          <li>
            <a href="{{route('LogoutProcess')}}">
              <i class="nc-icon nc-button-power"></i>
              <p>Logout</p>
            </a>
          </li>

        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="javascript:;">Dashboard Panel</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <form>
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split"></i>
                  </div>
                </div>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-settings-gear-65"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <span class="dropdown-item">Hi, <b>{{Session::get('username')}}</b></span>
                  <a class="dropdown-item" href="#">Profile</a>
                  <hr />
                  <a class="dropdown-item" href="{{route('LogoutProcess')}}">Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      @yield('content')

      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, BNN Group
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  {{-- <script src="{{asset('assets/js/core/jquery.min.js')}}"></script> --}}
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
  {{-- <script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script> --}}
  <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
  {{-- <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script> --}}
  <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <!-- Chart JS -->
  <script src="{{asset('assets/js/plugins/chartjs.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  {{-- <script src="{{asset('assets/js/paper-dashboard.min.js')}}" type="text/javascript"></script> --}}<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  {{-- Sweet Alert --}}
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  {{-- Demo Delete when unused --}}
  <script src="{{asset('assets/demo/demo.js')}}"></script>
  <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.17/js/bootstrap-select.min.js"></script> --}}
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.17/dist/js/bootstrap-select.min.js"></script>
  @stack('scripts')
  <script type="text/javascript">
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
  </script>
</body>

</html>
